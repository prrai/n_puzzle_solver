[Begin Solution]

[Program inputs]
ALGORITHM => A*
GAME => 15 puzzle
INPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/test/in_5.txt
OUTPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/test/out_5.txt
INITIAL STATE: 
[[1, 2, 3, 7], [6, 9, 4, 0], [5, 10, 8, 12], [13, 14, 11, 15]]
FINAL STATE:
[[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 0]]
HEURISTIC:  Manhattan_Distance
MOVES: 
L D L U U R R D L U L D L D R R D R 
PATH LENGTH: 19
NODES EXPANDED: 333
EXECUTION TIME:  0 seconds
TRAVERSED STATES: 
1 2 3 7 

6 9 4 0 

5 10 8 12 

13 14 11 15 

  |    
  V    
1 2 3 7 

6 9 0 4 

5 10 8 12 

13 14 11 15 

  |    
  V    
1 2 3 7 

6 9 8 4 

5 10 0 12 

13 14 11 15 

  |    
  V    
1 2 3 7 

6 9 8 4 

5 0 10 12 

13 14 11 15 

  |    
  V    
1 2 3 7 

6 0 8 4 

5 9 10 12 

13 14 11 15 

  |    
  V    
1 0 3 7 

6 2 8 4 

5 9 10 12 

13 14 11 15 

  |    
  V    
1 3 0 7 

6 2 8 4 

5 9 10 12 

13 14 11 15 

  |    
  V    
1 3 7 0 

6 2 8 4 

5 9 10 12 

13 14 11 15 

  |    
  V    
1 3 7 4 

6 2 8 0 

5 9 10 12 

13 14 11 15 

  |    
  V    
1 3 7 4 

6 2 0 8 

5 9 10 12 

13 14 11 15 

  |    
  V    
1 3 0 4 

6 2 7 8 

5 9 10 12 

13 14 11 15 

  |    
  V    
1 0 3 4 

6 2 7 8 

5 9 10 12 

13 14 11 15 

  |    
  V    
1 2 3 4 

6 0 7 8 

5 9 10 12 

13 14 11 15 

  |    
  V    
1 2 3 4 

0 6 7 8 

5 9 10 12 

13 14 11 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

0 9 10 12 

13 14 11 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 0 10 12 

13 14 11 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 0 12 

13 14 11 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 12 

13 14 0 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 12 

13 14 15 0 

[END Solution]

