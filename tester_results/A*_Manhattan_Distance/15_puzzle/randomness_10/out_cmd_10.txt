[Begin Solution]

[Program inputs]
ALGORITHM => A*
GAME => 15 puzzle
INPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/test/in_10.txt
OUTPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/test/out_10.txt
INITIAL STATE: 
[[0, 1, 2, 3], [5, 6, 7, 4], [9, 10, 11, 8], [13, 14, 15, 12]]
FINAL STATE:
[[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 0]]
HEURISTIC:  Manhattan_Distance
MOVES: 
R R R D D D 
PATH LENGTH: 7
NODES EXPANDED: 6
EXECUTION TIME:  0 seconds
TRAVERSED STATES: 
0 1 2 3 

5 6 7 4 

9 10 11 8 

13 14 15 12 

  |    
  V    
1 0 2 3 

5 6 7 4 

9 10 11 8 

13 14 15 12 

  |    
  V    
1 2 0 3 

5 6 7 4 

9 10 11 8 

13 14 15 12 

  |    
  V    
1 2 3 0 

5 6 7 4 

9 10 11 8 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 6 7 0 

9 10 11 8 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 0 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 12 

13 14 15 0 

[END Solution]

