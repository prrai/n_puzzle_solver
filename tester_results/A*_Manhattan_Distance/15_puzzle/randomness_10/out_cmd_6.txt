[Begin Solution]

[Program inputs]
ALGORITHM => A*
GAME => 15 puzzle
INPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/test/in_6.txt
OUTPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/test/out_6.txt
INITIAL STATE: 
[[1, 2, 3, 4], [5, 7, 11, 8], [9, 6, 0, 12], [13, 10, 14, 15]]
FINAL STATE:
[[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 0]]
HEURISTIC:  Manhattan_Distance
MOVES: 
U L D D R R 
PATH LENGTH: 7
NODES EXPANDED: 6
EXECUTION TIME:  0 seconds
TRAVERSED STATES: 
1 2 3 4 

5 7 11 8 

9 6 0 12 

13 10 14 15 

  |    
  V    
1 2 3 4 

5 7 0 8 

9 6 11 12 

13 10 14 15 

  |    
  V    
1 2 3 4 

5 0 7 8 

9 6 11 12 

13 10 14 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 0 11 12 

13 10 14 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 12 

13 0 14 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 12 

13 14 0 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 12 

13 14 15 0 

[END Solution]

