[Begin Solution]

[Program inputs]
ALGORITHM => A*
GAME => 15 puzzle
INPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/test/in_5.txt
OUTPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/test/out_5.txt
INITIAL STATE: 
[[2, 6, 3, 4], [5, 1, 7, 8], [0, 9, 10, 12], [13, 14, 11, 15]]
FINAL STATE:
[[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 0]]
HEURISTIC:  Manhattan_Distance
MOVES: 
U R U L D D R R D R 
PATH LENGTH: 11
NODES EXPANDED: 23
EXECUTION TIME:  0 seconds
TRAVERSED STATES: 
2 6 3 4 

5 1 7 8 

0 9 10 12 

13 14 11 15 

  |    
  V    
2 6 3 4 

0 1 7 8 

5 9 10 12 

13 14 11 15 

  |    
  V    
2 6 3 4 

1 0 7 8 

5 9 10 12 

13 14 11 15 

  |    
  V    
2 0 3 4 

1 6 7 8 

5 9 10 12 

13 14 11 15 

  |    
  V    
0 2 3 4 

1 6 7 8 

5 9 10 12 

13 14 11 15 

  |    
  V    
1 2 3 4 

0 6 7 8 

5 9 10 12 

13 14 11 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

0 9 10 12 

13 14 11 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 0 10 12 

13 14 11 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 0 12 

13 14 11 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 12 

13 14 0 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 12 

13 14 15 0 

[END Solution]

