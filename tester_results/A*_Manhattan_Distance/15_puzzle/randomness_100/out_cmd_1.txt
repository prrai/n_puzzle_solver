[Begin Solution]

[Program inputs]
ALGORITHM => A*
GAME => 15 puzzle
INPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/test/in_1.txt
OUTPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/test/out_1.txt
INITIAL STATE: 
[[1, 2, 3, 4], [5, 6, 7, 8], [10, 14, 11, 12], [13, 9, 15, 0]]
FINAL STATE:
[[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 0]]
HEURISTIC:  Manhattan_Distance
MOVES: 
L L U L D R R U L D L U R R D R 
PATH LENGTH: 17
NODES EXPANDED: 458
EXECUTION TIME:  0 seconds
TRAVERSED STATES: 
1 2 3 4 

5 6 7 8 

10 14 11 12 

13 9 15 0 

  |    
  V    
1 2 3 4 

5 6 7 8 

10 14 11 12 

13 9 0 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

10 14 11 12 

13 0 9 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

10 0 11 12 

13 14 9 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

0 10 11 12 

13 14 9 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

13 10 11 12 

0 14 9 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

13 10 11 12 

14 0 9 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

13 10 11 12 

14 9 0 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

13 10 0 12 

14 9 11 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

13 0 10 12 

14 9 11 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

13 9 10 12 

14 0 11 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

13 9 10 12 

0 14 11 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

0 9 10 12 

13 14 11 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 0 10 12 

13 14 11 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 0 12 

13 14 11 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 12 

13 14 0 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 12 

13 14 15 0 

[END Solution]

