[Begin Solution]

[Program inputs]
ALGORITHM => A*
GAME => 15 puzzle
INPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/test/in_5.txt
OUTPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/test/out_5.txt
INITIAL STATE: 
[[5, 1, 0, 4], [6, 3, 2, 7], [9, 10, 12, 8], [13, 14, 11, 15]]
FINAL STATE:
[[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 0]]
HEURISTIC:  Manhattan_Distance
MOVES: 
D L L U R R D R D L D R 
PATH LENGTH: 13
NODES EXPANDED: 17
EXECUTION TIME:  0 seconds
TRAVERSED STATES: 
5 1 0 4 

6 3 2 7 

9 10 12 8 

13 14 11 15 

  |    
  V    
5 1 2 4 

6 3 0 7 

9 10 12 8 

13 14 11 15 

  |    
  V    
5 1 2 4 

6 0 3 7 

9 10 12 8 

13 14 11 15 

  |    
  V    
5 1 2 4 

0 6 3 7 

9 10 12 8 

13 14 11 15 

  |    
  V    
0 1 2 4 

5 6 3 7 

9 10 12 8 

13 14 11 15 

  |    
  V    
1 0 2 4 

5 6 3 7 

9 10 12 8 

13 14 11 15 

  |    
  V    
1 2 0 4 

5 6 3 7 

9 10 12 8 

13 14 11 15 

  |    
  V    
1 2 3 4 

5 6 0 7 

9 10 12 8 

13 14 11 15 

  |    
  V    
1 2 3 4 

5 6 7 0 

9 10 12 8 

13 14 11 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 12 0 

13 14 11 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 0 12 

13 14 11 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 12 

13 14 0 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 12 

13 14 15 0 

[END Solution]

