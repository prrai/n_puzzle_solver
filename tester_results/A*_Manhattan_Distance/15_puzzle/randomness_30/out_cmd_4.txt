[Begin Solution]

[Program inputs]
ALGORITHM => A*
GAME => 15 puzzle
INPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/test/in_4.txt
OUTPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/test/out_4.txt
INITIAL STATE: 
[[0, 1, 4, 7], [5, 2, 6, 3], [9, 10, 11, 8], [13, 14, 15, 12]]
FINAL STATE:
[[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 0]]
HEURISTIC:  Manhattan_Distance
MOVES: 
R D R R U L D R D D 
PATH LENGTH: 11
NODES EXPANDED: 12
EXECUTION TIME:  0 seconds
TRAVERSED STATES: 
0 1 4 7 

5 2 6 3 

9 10 11 8 

13 14 15 12 

  |    
  V    
1 0 4 7 

5 2 6 3 

9 10 11 8 

13 14 15 12 

  |    
  V    
1 2 4 7 

5 0 6 3 

9 10 11 8 

13 14 15 12 

  |    
  V    
1 2 4 7 

5 6 0 3 

9 10 11 8 

13 14 15 12 

  |    
  V    
1 2 4 7 

5 6 3 0 

9 10 11 8 

13 14 15 12 

  |    
  V    
1 2 4 0 

5 6 3 7 

9 10 11 8 

13 14 15 12 

  |    
  V    
1 2 0 4 

5 6 3 7 

9 10 11 8 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 6 0 7 

9 10 11 8 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 6 7 0 

9 10 11 8 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 0 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 12 

13 14 15 0 

[END Solution]

