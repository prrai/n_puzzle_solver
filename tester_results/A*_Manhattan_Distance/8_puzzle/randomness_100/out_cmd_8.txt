[Begin Solution]

[Program inputs]
ALGORITHM => A*
GAME => 8 puzzle
INPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/test/in_8.txt
OUTPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/test/out_8.txt
INITIAL STATE: 
[[1, 6, 2], [3, 7, 8], [4, 5, 0]]
FINAL STATE:
[[1, 2, 3], [4, 5, 6], [7, 8, 0]]
HEURISTIC:  Manhattan_Distance
MOVES: 
L U L D R R U L U R D L D R 
PATH LENGTH: 15
NODES EXPANDED: 31
EXECUTION TIME:  0 seconds
TRAVERSED STATES: 
1 6 2 

3 7 8 

4 5 0 

  |    
  V    
1 6 2 

3 7 8 

4 0 5 

  |    
  V    
1 6 2 

3 0 8 

4 7 5 

  |    
  V    
1 6 2 

0 3 8 

4 7 5 

  |    
  V    
1 6 2 

4 3 8 

0 7 5 

  |    
  V    
1 6 2 

4 3 8 

7 0 5 

  |    
  V    
1 6 2 

4 3 8 

7 5 0 

  |    
  V    
1 6 2 

4 3 0 

7 5 8 

  |    
  V    
1 6 2 

4 0 3 

7 5 8 

  |    
  V    
1 0 2 

4 6 3 

7 5 8 

  |    
  V    
1 2 0 

4 6 3 

7 5 8 

  |    
  V    
1 2 3 

4 6 0 

7 5 8 

  |    
  V    
1 2 3 

4 0 6 

7 5 8 

  |    
  V    
1 2 3 

4 5 6 

7 0 8 

  |    
  V    
1 2 3 

4 5 6 

7 8 0 

[END Solution]

