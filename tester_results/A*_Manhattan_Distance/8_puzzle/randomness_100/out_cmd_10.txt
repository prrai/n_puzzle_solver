[Begin Solution]

[Program inputs]
ALGORITHM => A*
GAME => 8 puzzle
INPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/test/in_10.txt
OUTPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/test/out_10.txt
INITIAL STATE: 
[[2, 7, 4], [8, 1, 3], [0, 5, 6]]
FINAL STATE:
[[1, 2, 3], [4, 5, 6], [7, 8, 0]]
HEURISTIC:  Manhattan_Distance
MOVES: 
U R U L D R D L U U R R D L U L D R R D 
PATH LENGTH: 21
NODES EXPANDED: 452
EXECUTION TIME:  0 seconds
TRAVERSED STATES: 
2 7 4 

8 1 3 

0 5 6 

  |    
  V    
2 7 4 

0 1 3 

8 5 6 

  |    
  V    
2 7 4 

1 0 3 

8 5 6 

  |    
  V    
2 0 4 

1 7 3 

8 5 6 

  |    
  V    
0 2 4 

1 7 3 

8 5 6 

  |    
  V    
1 2 4 

0 7 3 

8 5 6 

  |    
  V    
1 2 4 

7 0 3 

8 5 6 

  |    
  V    
1 2 4 

7 5 3 

8 0 6 

  |    
  V    
1 2 4 

7 5 3 

0 8 6 

  |    
  V    
1 2 4 

0 5 3 

7 8 6 

  |    
  V    
0 2 4 

1 5 3 

7 8 6 

  |    
  V    
2 0 4 

1 5 3 

7 8 6 

  |    
  V    
2 4 0 

1 5 3 

7 8 6 

  |    
  V    
2 4 3 

1 5 0 

7 8 6 

  |    
  V    
2 4 3 

1 0 5 

7 8 6 

  |    
  V    
2 0 3 

1 4 5 

7 8 6 

  |    
  V    
0 2 3 

1 4 5 

7 8 6 

  |    
  V    
1 2 3 

0 4 5 

7 8 6 

  |    
  V    
1 2 3 

4 0 5 

7 8 6 

  |    
  V    
1 2 3 

4 5 0 

7 8 6 

  |    
  V    
1 2 3 

4 5 6 

7 8 0 

[END Solution]

