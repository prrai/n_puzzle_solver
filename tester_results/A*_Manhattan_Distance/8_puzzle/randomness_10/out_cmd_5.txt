[Begin Solution]

[Program inputs]
ALGORITHM => A*
GAME => 8 puzzle
INPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/test/in_5.txt
OUTPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/test/out_5.txt
INITIAL STATE: 
[[1, 2, 3], [4, 0, 6], [7, 5, 8]]
FINAL STATE:
[[1, 2, 3], [4, 5, 6], [7, 8, 0]]
HEURISTIC:  Manhattan_Distance
MOVES: 
D R 
PATH LENGTH: 3
NODES EXPANDED: 2
EXECUTION TIME:  0 seconds
TRAVERSED STATES: 
1 2 3 

4 0 6 

7 5 8 

  |    
  V    
1 2 3 

4 5 6 

7 0 8 

  |    
  V    
1 2 3 

4 5 6 

7 8 0 

[END Solution]

