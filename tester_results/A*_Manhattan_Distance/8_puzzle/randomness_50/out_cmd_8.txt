[Begin Solution]

[Program inputs]
ALGORITHM => A*
GAME => 8 puzzle
INPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/test/in_8.txt
OUTPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/test/out_8.txt
INITIAL STATE: 
[[2, 8, 5], [7, 0, 6], [1, 4, 3]]
FINAL STATE:
[[1, 2, 3], [4, 5, 6], [7, 8, 0]]
HEURISTIC:  Manhattan_Distance
MOVES: 
L D R U R D L U U R D L D R U L U L D D R R 
PATH LENGTH: 23
NODES EXPANDED: 1265
EXECUTION TIME:  0 seconds
TRAVERSED STATES: 
2 8 5 

7 0 6 

1 4 3 

  |    
  V    
2 8 5 

0 7 6 

1 4 3 

  |    
  V    
2 8 5 

1 7 6 

0 4 3 

  |    
  V    
2 8 5 

1 7 6 

4 0 3 

  |    
  V    
2 8 5 

1 0 6 

4 7 3 

  |    
  V    
2 8 5 

1 6 0 

4 7 3 

  |    
  V    
2 8 5 

1 6 3 

4 7 0 

  |    
  V    
2 8 5 

1 6 3 

4 0 7 

  |    
  V    
2 8 5 

1 0 3 

4 6 7 

  |    
  V    
2 0 5 

1 8 3 

4 6 7 

  |    
  V    
2 5 0 

1 8 3 

4 6 7 

  |    
  V    
2 5 3 

1 8 0 

4 6 7 

  |    
  V    
2 5 3 

1 0 8 

4 6 7 

  |    
  V    
2 5 3 

1 6 8 

4 0 7 

  |    
  V    
2 5 3 

1 6 8 

4 7 0 

  |    
  V    
2 5 3 

1 6 0 

4 7 8 

  |    
  V    
2 5 3 

1 0 6 

4 7 8 

  |    
  V    
2 0 3 

1 5 6 

4 7 8 

  |    
  V    
0 2 3 

1 5 6 

4 7 8 

  |    
  V    
1 2 3 

0 5 6 

4 7 8 

  |    
  V    
1 2 3 

4 5 6 

0 7 8 

  |    
  V    
1 2 3 

4 5 6 

7 0 8 

  |    
  V    
1 2 3 

4 5 6 

7 8 0 

[END Solution]

