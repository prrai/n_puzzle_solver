
[WARNING] Value for puzzle should ideally be 3 or 4.

[Begin Solution]

[Program inputs]
ALGORITHM => A*
GAME => 35 puzzle
INPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/test/in_4.txt
OUTPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/test/out_4.txt
INITIAL STATE: 
[[1, 2, 3, 4, 5, 6], [7, 8, 9, 0, 11, 16], [13, 14, 15, 10, 18, 12], [19, 20, 21, 22, 24, 30], [25, 33, 32, 17, 27, 23], [31, 26, 28, 34, 29, 35]]
FINAL STATE:
[[1, 2, 3, 4, 5, 6], [7, 8, 9, 10, 11, 12], [13, 14, 15, 16, 17, 18], [19, 20, 21, 22, 23, 24], [25, 26, 27, 28, 29, 30], [31, 32, 33, 34, 35, 0]]
HEURISTIC:  Manhattan_Distance
MOVES: 
R R D L U L D D D R R U L L U R D D D L L U L D R U R D R R 
PATH LENGTH: 31
NODES EXPANDED: 692
EXECUTION TIME:  0 seconds
TRAVERSED STATES: 
1 2 3 4 5 6 

7 8 9 0 11 16 

13 14 15 10 18 12 

19 20 21 22 24 30 

25 33 32 17 27 23 

31 26 28 34 29 35 

  |    
  V    
1 2 3 4 5 6 

7 8 9 11 0 16 

13 14 15 10 18 12 

19 20 21 22 24 30 

25 33 32 17 27 23 

31 26 28 34 29 35 

  |    
  V    
1 2 3 4 5 6 

7 8 9 11 16 0 

13 14 15 10 18 12 

19 20 21 22 24 30 

25 33 32 17 27 23 

31 26 28 34 29 35 

  |    
  V    
1 2 3 4 5 6 

7 8 9 11 16 12 

13 14 15 10 18 0 

19 20 21 22 24 30 

25 33 32 17 27 23 

31 26 28 34 29 35 

  |    
  V    
1 2 3 4 5 6 

7 8 9 11 16 12 

13 14 15 10 0 18 

19 20 21 22 24 30 

25 33 32 17 27 23 

31 26 28 34 29 35 

  |    
  V    
1 2 3 4 5 6 

7 8 9 11 0 12 

13 14 15 10 16 18 

19 20 21 22 24 30 

25 33 32 17 27 23 

31 26 28 34 29 35 

  |    
  V    
1 2 3 4 5 6 

7 8 9 0 11 12 

13 14 15 10 16 18 

19 20 21 22 24 30 

25 33 32 17 27 23 

31 26 28 34 29 35 

  |    
  V    
1 2 3 4 5 6 

7 8 9 10 11 12 

13 14 15 0 16 18 

19 20 21 22 24 30 

25 33 32 17 27 23 

31 26 28 34 29 35 

  |    
  V    
1 2 3 4 5 6 

7 8 9 10 11 12 

13 14 15 22 16 18 

19 20 21 0 24 30 

25 33 32 17 27 23 

31 26 28 34 29 35 

  |    
  V    
1 2 3 4 5 6 

7 8 9 10 11 12 

13 14 15 22 16 18 

19 20 21 17 24 30 

25 33 32 0 27 23 

31 26 28 34 29 35 

  |    
  V    
1 2 3 4 5 6 

7 8 9 10 11 12 

13 14 15 22 16 18 

19 20 21 17 24 30 

25 33 32 27 0 23 

31 26 28 34 29 35 

  |    
  V    
1 2 3 4 5 6 

7 8 9 10 11 12 

13 14 15 22 16 18 

19 20 21 17 24 30 

25 33 32 27 23 0 

31 26 28 34 29 35 

  |    
  V    
1 2 3 4 5 6 

7 8 9 10 11 12 

13 14 15 22 16 18 

19 20 21 17 24 0 

25 33 32 27 23 30 

31 26 28 34 29 35 

  |    
  V    
1 2 3 4 5 6 

7 8 9 10 11 12 

13 14 15 22 16 18 

19 20 21 17 0 24 

25 33 32 27 23 30 

31 26 28 34 29 35 

  |    
  V    
1 2 3 4 5 6 

7 8 9 10 11 12 

13 14 15 22 16 18 

19 20 21 0 17 24 

25 33 32 27 23 30 

31 26 28 34 29 35 

  |    
  V    
1 2 3 4 5 6 

7 8 9 10 11 12 

13 14 15 0 16 18 

19 20 21 22 17 24 

25 33 32 27 23 30 

31 26 28 34 29 35 

  |    
  V    
1 2 3 4 5 6 

7 8 9 10 11 12 

13 14 15 16 0 18 

19 20 21 22 17 24 

25 33 32 27 23 30 

31 26 28 34 29 35 

  |    
  V    
1 2 3 4 5 6 

7 8 9 10 11 12 

13 14 15 16 17 18 

19 20 21 22 0 24 

25 33 32 27 23 30 

31 26 28 34 29 35 

  |    
  V    
1 2 3 4 5 6 

7 8 9 10 11 12 

13 14 15 16 17 18 

19 20 21 22 23 24 

25 33 32 27 0 30 

31 26 28 34 29 35 

  |    
  V    
1 2 3 4 5 6 

7 8 9 10 11 12 

13 14 15 16 17 18 

19 20 21 22 23 24 

25 33 32 27 29 30 

31 26 28 34 0 35 

  |    
  V    
1 2 3 4 5 6 

7 8 9 10 11 12 

13 14 15 16 17 18 

19 20 21 22 23 24 

25 33 32 27 29 30 

31 26 28 0 34 35 

  |    
  V    
1 2 3 4 5 6 

7 8 9 10 11 12 

13 14 15 16 17 18 

19 20 21 22 23 24 

25 33 32 27 29 30 

31 26 0 28 34 35 

  |    
  V    
1 2 3 4 5 6 

7 8 9 10 11 12 

13 14 15 16 17 18 

19 20 21 22 23 24 

25 33 0 27 29 30 

31 26 32 28 34 35 

  |    
  V    
1 2 3 4 5 6 

7 8 9 10 11 12 

13 14 15 16 17 18 

19 20 21 22 23 24 

25 0 33 27 29 30 

31 26 32 28 34 35 

  |    
  V    
1 2 3 4 5 6 

7 8 9 10 11 12 

13 14 15 16 17 18 

19 20 21 22 23 24 

25 26 33 27 29 30 

31 0 32 28 34 35 

  |    
  V    
1 2 3 4 5 6 

7 8 9 10 11 12 

13 14 15 16 17 18 

19 20 21 22 23 24 

25 26 33 27 29 30 

31 32 0 28 34 35 

  |    
  V    
1 2 3 4 5 6 

7 8 9 10 11 12 

13 14 15 16 17 18 

19 20 21 22 23 24 

25 26 0 27 29 30 

31 32 33 28 34 35 

  |    
  V    
1 2 3 4 5 6 

7 8 9 10 11 12 

13 14 15 16 17 18 

19 20 21 22 23 24 

25 26 27 0 29 30 

31 32 33 28 34 35 

  |    
  V    
1 2 3 4 5 6 

7 8 9 10 11 12 

13 14 15 16 17 18 

19 20 21 22 23 24 

25 26 27 28 29 30 

31 32 33 0 34 35 

  |    
  V    
1 2 3 4 5 6 

7 8 9 10 11 12 

13 14 15 16 17 18 

19 20 21 22 23 24 

25 26 27 28 29 30 

31 32 33 34 0 35 

  |    
  V    
1 2 3 4 5 6 

7 8 9 10 11 12 

13 14 15 16 17 18 

19 20 21 22 23 24 

25 26 27 28 29 30 

31 32 33 34 35 0 

[END Solution]

