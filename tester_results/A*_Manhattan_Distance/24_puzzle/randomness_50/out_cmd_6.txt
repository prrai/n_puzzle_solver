
[WARNING] Value for puzzle should ideally be 3 or 4.

[Begin Solution]

[Program inputs]
ALGORITHM => A*
GAME => 24 puzzle
INPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/test/in_6.txt
OUTPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/test/out_6.txt
INITIAL STATE: 
[[1, 2, 8, 3, 5], [6, 7, 13, 4, 14], [11, 17, 10, 24, 9], [16, 18, 12, 0, 15], [21, 22, 23, 20, 19]]
FINAL STATE:
[[1, 2, 3, 4, 5], [6, 7, 8, 9, 10], [11, 12, 13, 14, 15], [16, 17, 18, 19, 20], [21, 22, 23, 24, 0]]
HEURISTIC:  Manhattan_Distance
MOVES: 
U L D L U R U U R D D R U L D R D D L U R D 
PATH LENGTH: 23
NODES EXPANDED: 82
EXECUTION TIME:  0 seconds
TRAVERSED STATES: 
1 2 8 3 5 

6 7 13 4 14 

11 17 10 24 9 

16 18 12 0 15 

21 22 23 20 19 

  |    
  V    
1 2 8 3 5 

6 7 13 4 14 

11 17 10 0 9 

16 18 12 24 15 

21 22 23 20 19 

  |    
  V    
1 2 8 3 5 

6 7 13 4 14 

11 17 0 10 9 

16 18 12 24 15 

21 22 23 20 19 

  |    
  V    
1 2 8 3 5 

6 7 13 4 14 

11 17 12 10 9 

16 18 0 24 15 

21 22 23 20 19 

  |    
  V    
1 2 8 3 5 

6 7 13 4 14 

11 17 12 10 9 

16 0 18 24 15 

21 22 23 20 19 

  |    
  V    
1 2 8 3 5 

6 7 13 4 14 

11 0 12 10 9 

16 17 18 24 15 

21 22 23 20 19 

  |    
  V    
1 2 8 3 5 

6 7 13 4 14 

11 12 0 10 9 

16 17 18 24 15 

21 22 23 20 19 

  |    
  V    
1 2 8 3 5 

6 7 0 4 14 

11 12 13 10 9 

16 17 18 24 15 

21 22 23 20 19 

  |    
  V    
1 2 0 3 5 

6 7 8 4 14 

11 12 13 10 9 

16 17 18 24 15 

21 22 23 20 19 

  |    
  V    
1 2 3 0 5 

6 7 8 4 14 

11 12 13 10 9 

16 17 18 24 15 

21 22 23 20 19 

  |    
  V    
1 2 3 4 5 

6 7 8 0 14 

11 12 13 10 9 

16 17 18 24 15 

21 22 23 20 19 

  |    
  V    
1 2 3 4 5 

6 7 8 10 14 

11 12 13 0 9 

16 17 18 24 15 

21 22 23 20 19 

  |    
  V    
1 2 3 4 5 

6 7 8 10 14 

11 12 13 9 0 

16 17 18 24 15 

21 22 23 20 19 

  |    
  V    
1 2 3 4 5 

6 7 8 10 0 

11 12 13 9 14 

16 17 18 24 15 

21 22 23 20 19 

  |    
  V    
1 2 3 4 5 

6 7 8 0 10 

11 12 13 9 14 

16 17 18 24 15 

21 22 23 20 19 

  |    
  V    
1 2 3 4 5 

6 7 8 9 10 

11 12 13 0 14 

16 17 18 24 15 

21 22 23 20 19 

  |    
  V    
1 2 3 4 5 

6 7 8 9 10 

11 12 13 14 0 

16 17 18 24 15 

21 22 23 20 19 

  |    
  V    
1 2 3 4 5 

6 7 8 9 10 

11 12 13 14 15 

16 17 18 24 0 

21 22 23 20 19 

  |    
  V    
1 2 3 4 5 

6 7 8 9 10 

11 12 13 14 15 

16 17 18 24 19 

21 22 23 20 0 

  |    
  V    
1 2 3 4 5 

6 7 8 9 10 

11 12 13 14 15 

16 17 18 24 19 

21 22 23 0 20 

  |    
  V    
1 2 3 4 5 

6 7 8 9 10 

11 12 13 14 15 

16 17 18 0 19 

21 22 23 24 20 

  |    
  V    
1 2 3 4 5 

6 7 8 9 10 

11 12 13 14 15 

16 17 18 19 0 

21 22 23 24 20 

  |    
  V    
1 2 3 4 5 

6 7 8 9 10 

11 12 13 14 15 

16 17 18 19 20 

21 22 23 24 0 

[END Solution]

