
[WARNING] Value for puzzle should ideally be 3 or 4.

[Begin Solution]

[Program inputs]
ALGORITHM => A*
GAME => 24 puzzle
INPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/test/in_8.txt
OUTPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/test/out_8.txt
INITIAL STATE: 
[[0, 2, 3, 4, 5], [1, 7, 8, 9, 10], [6, 11, 13, 14, 15], [16, 12, 17, 19, 20], [21, 22, 18, 23, 24]]
FINAL STATE:
[[1, 2, 3, 4, 5], [6, 7, 8, 9, 10], [11, 12, 13, 14, 15], [16, 17, 18, 19, 20], [21, 22, 23, 24, 0]]
HEURISTIC:  Manhattan_Distance
MOVES: 
D D R D R D R R 
PATH LENGTH: 9
NODES EXPANDED: 8
EXECUTION TIME:  0 seconds
TRAVERSED STATES: 
0 2 3 4 5 

1 7 8 9 10 

6 11 13 14 15 

16 12 17 19 20 

21 22 18 23 24 

  |    
  V    
1 2 3 4 5 

0 7 8 9 10 

6 11 13 14 15 

16 12 17 19 20 

21 22 18 23 24 

  |    
  V    
1 2 3 4 5 

6 7 8 9 10 

0 11 13 14 15 

16 12 17 19 20 

21 22 18 23 24 

  |    
  V    
1 2 3 4 5 

6 7 8 9 10 

11 0 13 14 15 

16 12 17 19 20 

21 22 18 23 24 

  |    
  V    
1 2 3 4 5 

6 7 8 9 10 

11 12 13 14 15 

16 0 17 19 20 

21 22 18 23 24 

  |    
  V    
1 2 3 4 5 

6 7 8 9 10 

11 12 13 14 15 

16 17 0 19 20 

21 22 18 23 24 

  |    
  V    
1 2 3 4 5 

6 7 8 9 10 

11 12 13 14 15 

16 17 18 19 20 

21 22 0 23 24 

  |    
  V    
1 2 3 4 5 

6 7 8 9 10 

11 12 13 14 15 

16 17 18 19 20 

21 22 23 0 24 

  |    
  V    
1 2 3 4 5 

6 7 8 9 10 

11 12 13 14 15 

16 17 18 19 20 

21 22 23 24 0 

[END Solution]

