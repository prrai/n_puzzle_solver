
[WARNING] Value for puzzle should ideally be 3 or 4.

[Begin Solution]

[Program inputs]
ALGORITHM => A*
GAME => 24 puzzle
INPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/test/in_5.txt
OUTPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/test/out_5.txt
INITIAL STATE: 
[[1, 2, 3, 4, 5], [6, 0, 8, 9, 10], [11, 7, 12, 13, 15], [16, 17, 18, 14, 20], [21, 22, 23, 19, 24]]
FINAL STATE:
[[1, 2, 3, 4, 5], [6, 7, 8, 9, 10], [11, 12, 13, 14, 15], [16, 17, 18, 19, 20], [21, 22, 23, 24, 0]]
HEURISTIC:  Manhattan_Distance
MOVES: 
D R R D D R 
PATH LENGTH: 7
NODES EXPANDED: 6
EXECUTION TIME:  0 seconds
TRAVERSED STATES: 
1 2 3 4 5 

6 0 8 9 10 

11 7 12 13 15 

16 17 18 14 20 

21 22 23 19 24 

  |    
  V    
1 2 3 4 5 

6 7 8 9 10 

11 0 12 13 15 

16 17 18 14 20 

21 22 23 19 24 

  |    
  V    
1 2 3 4 5 

6 7 8 9 10 

11 12 0 13 15 

16 17 18 14 20 

21 22 23 19 24 

  |    
  V    
1 2 3 4 5 

6 7 8 9 10 

11 12 13 0 15 

16 17 18 14 20 

21 22 23 19 24 

  |    
  V    
1 2 3 4 5 

6 7 8 9 10 

11 12 13 14 15 

16 17 18 0 20 

21 22 23 19 24 

  |    
  V    
1 2 3 4 5 

6 7 8 9 10 

11 12 13 14 15 

16 17 18 19 20 

21 22 23 0 24 

  |    
  V    
1 2 3 4 5 

6 7 8 9 10 

11 12 13 14 15 

16 17 18 19 20 

21 22 23 24 0 

[END Solution]

