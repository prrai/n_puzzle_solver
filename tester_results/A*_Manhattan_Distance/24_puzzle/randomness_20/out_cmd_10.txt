
[WARNING] Value for puzzle should ideally be 3 or 4.

[Begin Solution]

[Program inputs]
ALGORITHM => A*
GAME => 24 puzzle
INPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/test/in_10.txt
OUTPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/test/out_10.txt
INITIAL STATE: 
[[1, 2, 3, 5, 0], [6, 7, 8, 4, 10], [11, 12, 13, 9, 14], [16, 17, 23, 18, 15], [21, 22, 19, 24, 20]]
FINAL STATE:
[[1, 2, 3, 4, 5], [6, 7, 8, 9, 10], [11, 12, 13, 14, 15], [16, 17, 18, 19, 20], [21, 22, 23, 24, 0]]
HEURISTIC:  Manhattan_Distance
MOVES: 
L D D R D D L L U R D R 
PATH LENGTH: 13
NODES EXPANDED: 20
EXECUTION TIME:  0 seconds
TRAVERSED STATES: 
1 2 3 5 0 

6 7 8 4 10 

11 12 13 9 14 

16 17 23 18 15 

21 22 19 24 20 

  |    
  V    
1 2 3 0 5 

6 7 8 4 10 

11 12 13 9 14 

16 17 23 18 15 

21 22 19 24 20 

  |    
  V    
1 2 3 4 5 

6 7 8 0 10 

11 12 13 9 14 

16 17 23 18 15 

21 22 19 24 20 

  |    
  V    
1 2 3 4 5 

6 7 8 9 10 

11 12 13 0 14 

16 17 23 18 15 

21 22 19 24 20 

  |    
  V    
1 2 3 4 5 

6 7 8 9 10 

11 12 13 14 0 

16 17 23 18 15 

21 22 19 24 20 

  |    
  V    
1 2 3 4 5 

6 7 8 9 10 

11 12 13 14 15 

16 17 23 18 0 

21 22 19 24 20 

  |    
  V    
1 2 3 4 5 

6 7 8 9 10 

11 12 13 14 15 

16 17 23 18 20 

21 22 19 24 0 

  |    
  V    
1 2 3 4 5 

6 7 8 9 10 

11 12 13 14 15 

16 17 23 18 20 

21 22 19 0 24 

  |    
  V    
1 2 3 4 5 

6 7 8 9 10 

11 12 13 14 15 

16 17 23 18 20 

21 22 0 19 24 

  |    
  V    
1 2 3 4 5 

6 7 8 9 10 

11 12 13 14 15 

16 17 0 18 20 

21 22 23 19 24 

  |    
  V    
1 2 3 4 5 

6 7 8 9 10 

11 12 13 14 15 

16 17 18 0 20 

21 22 23 19 24 

  |    
  V    
1 2 3 4 5 

6 7 8 9 10 

11 12 13 14 15 

16 17 18 19 20 

21 22 23 0 24 

  |    
  V    
1 2 3 4 5 

6 7 8 9 10 

11 12 13 14 15 

16 17 18 19 20 

21 22 23 24 0 

[END Solution]

