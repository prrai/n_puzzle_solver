[Begin Solution]

[Program inputs]
ALGORITHM => IDA*
GAME => 8 puzzle
INPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/randomness_100/in_10.txt
OUTPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/randomness_100/out_10.txt
INITIAL STATE: 
[[0, 2, 6], [4, 1, 8], [5, 7, 3]]
FINAL STATE:
[[1, 2, 3], [4, 5, 6], [7, 8, 0]]
HEURISTIC:  Manhattan_Distance
MOVES: 
D D R R U U L D D R U L L U R R D D 
PATH LENGTH: 19
NODES EXPANDED: 28026
EXECUTION TIME:  1 seconds
TRAVERSED STATES: 
0 2 6 

4 1 8 

5 7 3 

  |    
  V    
4 2 6 

0 1 8 

5 7 3 

  |    
  V    
4 2 6 

5 1 8 

0 7 3 

  |    
  V    
4 2 6 

5 1 8 

7 0 3 

  |    
  V    
4 2 6 

5 1 8 

7 3 0 

  |    
  V    
4 2 6 

5 1 0 

7 3 8 

  |    
  V    
4 2 0 

5 1 6 

7 3 8 

  |    
  V    
4 0 2 

5 1 6 

7 3 8 

  |    
  V    
4 1 2 

5 0 6 

7 3 8 

  |    
  V    
4 1 2 

5 3 6 

7 0 8 

  |    
  V    
4 1 2 

5 3 6 

7 8 0 

  |    
  V    
4 1 2 

5 3 0 

7 8 6 

  |    
  V    
4 1 2 

5 0 3 

7 8 6 

  |    
  V    
4 1 2 

0 5 3 

7 8 6 

  |    
  V    
0 1 2 

4 5 3 

7 8 6 

  |    
  V    
1 0 2 

4 5 3 

7 8 6 

  |    
  V    
1 2 0 

4 5 3 

7 8 6 

  |    
  V    
1 2 3 

4 5 0 

7 8 6 

  |    
  V    
1 2 3 

4 5 6 

7 8 0 

[END Solution]

