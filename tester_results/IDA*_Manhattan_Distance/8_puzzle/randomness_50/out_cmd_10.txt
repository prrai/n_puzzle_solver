[Begin Solution]

[Program inputs]
ALGORITHM => IDA*
GAME => 8 puzzle
INPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/randomness_50/in_10.txt
OUTPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/randomness_50/out_10.txt
INITIAL STATE: 
[[1, 5, 3], [4, 8, 6], [7, 2, 0]]
FINAL STATE:
[[1, 2, 3], [4, 5, 6], [7, 8, 0]]
HEURISTIC:  Manhattan_Distance
MOVES: 
L U U R D D L U R U L D D R 
PATH LENGTH: 15
NODES EXPANDED: 16959
EXECUTION TIME:  1 seconds
TRAVERSED STATES: 
1 5 3 

4 8 6 

7 2 0 

  |    
  V    
1 5 3 

4 8 6 

7 0 2 

  |    
  V    
1 5 3 

4 0 6 

7 8 2 

  |    
  V    
1 0 3 

4 5 6 

7 8 2 

  |    
  V    
1 3 0 

4 5 6 

7 8 2 

  |    
  V    
1 3 6 

4 5 0 

7 8 2 

  |    
  V    
1 3 6 

4 5 2 

7 8 0 

  |    
  V    
1 3 6 

4 5 2 

7 0 8 

  |    
  V    
1 3 6 

4 0 2 

7 5 8 

  |    
  V    
1 3 6 

4 2 0 

7 5 8 

  |    
  V    
1 3 0 

4 2 6 

7 5 8 

  |    
  V    
1 0 3 

4 2 6 

7 5 8 

  |    
  V    
1 2 3 

4 0 6 

7 5 8 

  |    
  V    
1 2 3 

4 5 6 

7 0 8 

  |    
  V    
1 2 3 

4 5 6 

7 8 0 

[END Solution]

