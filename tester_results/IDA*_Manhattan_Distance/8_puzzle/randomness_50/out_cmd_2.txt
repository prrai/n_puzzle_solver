[Begin Solution]

[Program inputs]
ALGORITHM => IDA*
GAME => 8 puzzle
INPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/randomness_50/in_2.txt
OUTPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/randomness_50/out_2.txt
INITIAL STATE: 
[[4, 5, 1], [8, 7, 2], [0, 6, 3]]
FINAL STATE:
[[1, 2, 3], [4, 5, 6], [7, 8, 0]]
HEURISTIC:  Manhattan_Distance
MOVES: 
U R U R D D L L U U R R D D 
PATH LENGTH: 15
NODES EXPANDED: 17
EXECUTION TIME:  0 seconds
TRAVERSED STATES: 
4 5 1 

8 7 2 

0 6 3 

  |    
  V    
4 5 1 

0 7 2 

8 6 3 

  |    
  V    
4 5 1 

7 0 2 

8 6 3 

  |    
  V    
4 0 1 

7 5 2 

8 6 3 

  |    
  V    
4 1 0 

7 5 2 

8 6 3 

  |    
  V    
4 1 2 

7 5 0 

8 6 3 

  |    
  V    
4 1 2 

7 5 3 

8 6 0 

  |    
  V    
4 1 2 

7 5 3 

8 0 6 

  |    
  V    
4 1 2 

7 5 3 

0 8 6 

  |    
  V    
4 1 2 

0 5 3 

7 8 6 

  |    
  V    
0 1 2 

4 5 3 

7 8 6 

  |    
  V    
1 0 2 

4 5 3 

7 8 6 

  |    
  V    
1 2 0 

4 5 3 

7 8 6 

  |    
  V    
1 2 3 

4 5 0 

7 8 6 

  |    
  V    
1 2 3 

4 5 6 

7 8 0 

[END Solution]

