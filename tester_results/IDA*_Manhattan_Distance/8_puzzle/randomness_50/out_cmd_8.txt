[Begin Solution]

[Program inputs]
ALGORITHM => IDA*
GAME => 8 puzzle
INPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/randomness_50/in_8.txt
OUTPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/randomness_50/out_8.txt
INITIAL STATE: 
[[7, 2, 5], [3, 1, 6], [0, 4, 8]]
FINAL STATE:
[[1, 2, 3], [4, 5, 6], [7, 8, 0]]
HEURISTIC:  Manhattan_Distance
MOVES: 
R U L U R D D L U R D R U U L L D R R D 
PATH LENGTH: 21
NODES EXPANDED: 31288
EXECUTION TIME:  1 seconds
TRAVERSED STATES: 
7 2 5 

3 1 6 

0 4 8 

  |    
  V    
7 2 5 

3 1 6 

4 0 8 

  |    
  V    
7 2 5 

3 0 6 

4 1 8 

  |    
  V    
7 2 5 

0 3 6 

4 1 8 

  |    
  V    
0 2 5 

7 3 6 

4 1 8 

  |    
  V    
2 0 5 

7 3 6 

4 1 8 

  |    
  V    
2 3 5 

7 0 6 

4 1 8 

  |    
  V    
2 3 5 

7 1 6 

4 0 8 

  |    
  V    
2 3 5 

7 1 6 

0 4 8 

  |    
  V    
2 3 5 

0 1 6 

7 4 8 

  |    
  V    
2 3 5 

1 0 6 

7 4 8 

  |    
  V    
2 3 5 

1 4 6 

7 0 8 

  |    
  V    
2 3 5 

1 4 6 

7 8 0 

  |    
  V    
2 3 5 

1 4 0 

7 8 6 

  |    
  V    
2 3 0 

1 4 5 

7 8 6 

  |    
  V    
2 0 3 

1 4 5 

7 8 6 

  |    
  V    
0 2 3 

1 4 5 

7 8 6 

  |    
  V    
1 2 3 

0 4 5 

7 8 6 

  |    
  V    
1 2 3 

4 0 5 

7 8 6 

  |    
  V    
1 2 3 

4 5 0 

7 8 6 

  |    
  V    
1 2 3 

4 5 6 

7 8 0 

[END Solution]

