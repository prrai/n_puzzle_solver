[Begin Solution]

[Program inputs]
ALGORITHM => IDA*
GAME => 8 puzzle
INPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/randomness_20/in_2.txt
OUTPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/randomness_20/out_2.txt
INITIAL STATE: 
[[0, 1, 2], [4, 5, 3], [7, 8, 6]]
FINAL STATE:
[[1, 2, 3], [4, 5, 6], [7, 8, 0]]
HEURISTIC:  Manhattan_Distance
MOVES: 
R R D D 
PATH LENGTH: 5
NODES EXPANDED: 4
EXECUTION TIME:  0 seconds
TRAVERSED STATES: 
0 1 2 

4 5 3 

7 8 6 

  |    
  V    
1 0 2 

4 5 3 

7 8 6 

  |    
  V    
1 2 0 

4 5 3 

7 8 6 

  |    
  V    
1 2 3 

4 5 0 

7 8 6 

  |    
  V    
1 2 3 

4 5 6 

7 8 0 

[END Solution]

