[Begin Solution]

[Program inputs]
ALGORITHM => IDA*
GAME => 8 puzzle
INPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/randomness_20/in_10.txt
OUTPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/randomness_20/out_10.txt
INITIAL STATE: 
[[1, 2, 0], [5, 6, 3], [4, 7, 8]]
FINAL STATE:
[[1, 2, 3], [4, 5, 6], [7, 8, 0]]
HEURISTIC:  Manhattan_Distance
MOVES: 
D L L D R R 
PATH LENGTH: 7
NODES EXPANDED: 6
EXECUTION TIME:  0 seconds
TRAVERSED STATES: 
1 2 0 

5 6 3 

4 7 8 

  |    
  V    
1 2 3 

5 6 0 

4 7 8 

  |    
  V    
1 2 3 

5 0 6 

4 7 8 

  |    
  V    
1 2 3 

0 5 6 

4 7 8 

  |    
  V    
1 2 3 

4 5 6 

0 7 8 

  |    
  V    
1 2 3 

4 5 6 

7 0 8 

  |    
  V    
1 2 3 

4 5 6 

7 8 0 

[END Solution]

