[Begin Solution]

[Program inputs]
ALGORITHM => IDA*
GAME => 8 puzzle
INPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/randomness_20/in_5.txt
OUTPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/randomness_20/out_5.txt
INITIAL STATE: 
[[2, 4, 3], [1, 0, 5], [7, 8, 6]]
FINAL STATE:
[[1, 2, 3], [4, 5, 6], [7, 8, 0]]
HEURISTIC:  Manhattan_Distance
MOVES: 
U L D R R D 
PATH LENGTH: 7
NODES EXPANDED: 6
EXECUTION TIME:  0 seconds
TRAVERSED STATES: 
2 4 3 

1 0 5 

7 8 6 

  |    
  V    
2 0 3 

1 4 5 

7 8 6 

  |    
  V    
0 2 3 

1 4 5 

7 8 6 

  |    
  V    
1 2 3 

0 4 5 

7 8 6 

  |    
  V    
1 2 3 

4 0 5 

7 8 6 

  |    
  V    
1 2 3 

4 5 0 

7 8 6 

  |    
  V    
1 2 3 

4 5 6 

7 8 0 

[END Solution]

