[Begin Solution]

[Program inputs]
ALGORITHM => IDA*
GAME => 8 puzzle
INPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/randomness_20/in_8.txt
OUTPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/randomness_20/out_8.txt
INITIAL STATE: 
[[1, 2, 3], [8, 0, 5], [4, 7, 6]]
FINAL STATE:
[[1, 2, 3], [4, 5, 6], [7, 8, 0]]
HEURISTIC:  Manhattan_Distance
MOVES: 
L D R U R D 
PATH LENGTH: 7
NODES EXPANDED: 6
EXECUTION TIME:  0 seconds
TRAVERSED STATES: 
1 2 3 

8 0 5 

4 7 6 

  |    
  V    
1 2 3 

0 8 5 

4 7 6 

  |    
  V    
1 2 3 

4 8 5 

0 7 6 

  |    
  V    
1 2 3 

4 8 5 

7 0 6 

  |    
  V    
1 2 3 

4 0 5 

7 8 6 

  |    
  V    
1 2 3 

4 5 0 

7 8 6 

  |    
  V    
1 2 3 

4 5 6 

7 8 0 

[END Solution]

