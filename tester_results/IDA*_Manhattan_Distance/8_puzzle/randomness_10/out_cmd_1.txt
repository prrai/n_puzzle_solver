[Begin Solution]

[Program inputs]
ALGORITHM => IDA*
GAME => 8 puzzle
INPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/randomness_10/in_1.txt
OUTPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/randomness_10/out_1.txt
INITIAL STATE: 
[[1, 2, 3], [4, 5, 6], [7, 8, 0]]
FINAL STATE:
[[1, 2, 3], [4, 5, 6], [7, 8, 0]]
HEURISTIC:  Manhattan_Distance
MOVES: 

PATH LENGTH: 1
NODES EXPANDED: 0
EXECUTION TIME:  0 seconds
TRAVERSED STATES: 
1 2 3 

4 5 6 

7 8 0 

[END Solution]

