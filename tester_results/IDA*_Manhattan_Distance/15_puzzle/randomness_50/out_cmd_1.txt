[Begin Solution]

[Program inputs]
ALGORITHM => IDA*
GAME => 15 puzzle
INPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/randomness_50/in_1.txt
OUTPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/randomness_50/out_1.txt
INITIAL STATE: 
[[1, 6, 2, 4], [10, 0, 3, 12], [5, 13, 8, 7], [14, 9, 11, 15]]
FINAL STATE:
[[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 0]]
HEURISTIC:  Manhattan_Distance
MOVES: 
L D R D L U R U U R D D R U L D D R 
PATH LENGTH: 19
NODES EXPANDED: 63
EXECUTION TIME:  0 seconds
TRAVERSED STATES: 
1 6 2 4 

10 0 3 12 

5 13 8 7 

14 9 11 15 

  |    
  V    
1 6 2 4 

0 10 3 12 

5 13 8 7 

14 9 11 15 

  |    
  V    
1 6 2 4 

5 10 3 12 

0 13 8 7 

14 9 11 15 

  |    
  V    
1 6 2 4 

5 10 3 12 

13 0 8 7 

14 9 11 15 

  |    
  V    
1 6 2 4 

5 10 3 12 

13 9 8 7 

14 0 11 15 

  |    
  V    
1 6 2 4 

5 10 3 12 

13 9 8 7 

0 14 11 15 

  |    
  V    
1 6 2 4 

5 10 3 12 

0 9 8 7 

13 14 11 15 

  |    
  V    
1 6 2 4 

5 10 3 12 

9 0 8 7 

13 14 11 15 

  |    
  V    
1 6 2 4 

5 0 3 12 

9 10 8 7 

13 14 11 15 

  |    
  V    
1 0 2 4 

5 6 3 12 

9 10 8 7 

13 14 11 15 

  |    
  V    
1 2 0 4 

5 6 3 12 

9 10 8 7 

13 14 11 15 

  |    
  V    
1 2 3 4 

5 6 0 12 

9 10 8 7 

13 14 11 15 

  |    
  V    
1 2 3 4 

5 6 8 12 

9 10 0 7 

13 14 11 15 

  |    
  V    
1 2 3 4 

5 6 8 12 

9 10 7 0 

13 14 11 15 

  |    
  V    
1 2 3 4 

5 6 8 0 

9 10 7 12 

13 14 11 15 

  |    
  V    
1 2 3 4 

5 6 0 8 

9 10 7 12 

13 14 11 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 0 12 

13 14 11 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 12 

13 14 0 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 12 

13 14 15 0 

[END Solution]

