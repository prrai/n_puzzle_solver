[Begin Solution]

[Program inputs]
ALGORITHM => IDA*
GAME => 15 puzzle
INPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/randomness_50/in_9.txt
OUTPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/randomness_50/out_9.txt
INITIAL STATE: 
[[1, 2, 3, 4], [9, 13, 11, 8], [5, 14, 6, 7], [10, 0, 15, 12]]
FINAL STATE:
[[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 0]]
HEURISTIC:  Manhattan_Distance
MOVES: 
U U L D R U R D R D L L L U R U R D L D R R 
PATH LENGTH: 23
NODES EXPANDED: 295392
EXECUTION TIME: 15 seconds
TRAVERSED STATES: 
1 2 3 4 

9 13 11 8 

5 14 6 7 

10 0 15 12 

  |    
  V    
1 2 3 4 

9 13 11 8 

5 0 6 7 

10 14 15 12 

  |    
  V    
1 2 3 4 

9 0 11 8 

5 13 6 7 

10 14 15 12 

  |    
  V    
1 2 3 4 

0 9 11 8 

5 13 6 7 

10 14 15 12 

  |    
  V    
1 2 3 4 

5 9 11 8 

0 13 6 7 

10 14 15 12 

  |    
  V    
1 2 3 4 

5 9 11 8 

13 0 6 7 

10 14 15 12 

  |    
  V    
1 2 3 4 

5 0 11 8 

13 9 6 7 

10 14 15 12 

  |    
  V    
1 2 3 4 

5 11 0 8 

13 9 6 7 

10 14 15 12 

  |    
  V    
1 2 3 4 

5 11 6 8 

13 9 0 7 

10 14 15 12 

  |    
  V    
1 2 3 4 

5 11 6 8 

13 9 7 0 

10 14 15 12 

  |    
  V    
1 2 3 4 

5 11 6 8 

13 9 7 12 

10 14 15 0 

  |    
  V    
1 2 3 4 

5 11 6 8 

13 9 7 12 

10 14 0 15 

  |    
  V    
1 2 3 4 

5 11 6 8 

13 9 7 12 

10 0 14 15 

  |    
  V    
1 2 3 4 

5 11 6 8 

13 9 7 12 

0 10 14 15 

  |    
  V    
1 2 3 4 

5 11 6 8 

0 9 7 12 

13 10 14 15 

  |    
  V    
1 2 3 4 

5 11 6 8 

9 0 7 12 

13 10 14 15 

  |    
  V    
1 2 3 4 

5 0 6 8 

9 11 7 12 

13 10 14 15 

  |    
  V    
1 2 3 4 

5 6 0 8 

9 11 7 12 

13 10 14 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 11 0 12 

13 10 14 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 0 11 12 

13 10 14 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 12 

13 0 14 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 12 

13 14 0 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 12 

13 14 15 0 

[END Solution]

