[Begin Solution]

[Program inputs]
ALGORITHM => IDA*
GAME => 15 puzzle
INPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/randomness_20/in_3.txt
OUTPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/randomness_20/out_3.txt
INITIAL STATE: 
[[1, 2, 4, 8], [5, 7, 10, 3], [9, 6, 0, 11], [13, 14, 15, 12]]
FINAL STATE:
[[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 0]]
HEURISTIC:  Manhattan_Distance
MOVES: 
U R U L D L D R R D 
PATH LENGTH: 11
NODES EXPANDED: 28
EXECUTION TIME:  0 seconds
TRAVERSED STATES: 
1 2 4 8 

5 7 10 3 

9 6 0 11 

13 14 15 12 

  |    
  V    
1 2 4 8 

5 7 0 3 

9 6 10 11 

13 14 15 12 

  |    
  V    
1 2 4 8 

5 7 3 0 

9 6 10 11 

13 14 15 12 

  |    
  V    
1 2 4 0 

5 7 3 8 

9 6 10 11 

13 14 15 12 

  |    
  V    
1 2 0 4 

5 7 3 8 

9 6 10 11 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 7 0 8 

9 6 10 11 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 0 7 8 

9 6 10 11 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 0 10 11 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 0 11 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 0 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 12 

13 14 15 0 

[END Solution]

