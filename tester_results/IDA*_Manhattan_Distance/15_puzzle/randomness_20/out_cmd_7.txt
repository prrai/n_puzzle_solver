[Begin Solution]

[Program inputs]
ALGORITHM => IDA*
GAME => 15 puzzle
INPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/randomness_20/in_7.txt
OUTPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/randomness_20/out_7.txt
INITIAL STATE: 
[[1, 2, 3, 4], [9, 5, 7, 8], [10, 11, 0, 12], [6, 13, 14, 15]]
FINAL STATE:
[[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 0]]
HEURISTIC:  Manhattan_Distance
MOVES: 
L L D R U L U R D D R R 
PATH LENGTH: 13
NODES EXPANDED: 101
EXECUTION TIME:  0 seconds
TRAVERSED STATES: 
1 2 3 4 

9 5 7 8 

10 11 0 12 

6 13 14 15 

  |    
  V    
1 2 3 4 

9 5 7 8 

10 0 11 12 

6 13 14 15 

  |    
  V    
1 2 3 4 

9 5 7 8 

0 10 11 12 

6 13 14 15 

  |    
  V    
1 2 3 4 

9 5 7 8 

6 10 11 12 

0 13 14 15 

  |    
  V    
1 2 3 4 

9 5 7 8 

6 10 11 12 

13 0 14 15 

  |    
  V    
1 2 3 4 

9 5 7 8 

6 0 11 12 

13 10 14 15 

  |    
  V    
1 2 3 4 

9 5 7 8 

0 6 11 12 

13 10 14 15 

  |    
  V    
1 2 3 4 

0 5 7 8 

9 6 11 12 

13 10 14 15 

  |    
  V    
1 2 3 4 

5 0 7 8 

9 6 11 12 

13 10 14 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 0 11 12 

13 10 14 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 12 

13 0 14 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 12 

13 14 0 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 12 

13 14 15 0 

[END Solution]

