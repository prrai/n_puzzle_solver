[Begin Solution]

[Program inputs]
ALGORITHM => IDA*
GAME => 15 puzzle
INPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/randomness_100/in_2.txt
OUTPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/randomness_100/out_2.txt
INITIAL STATE: 
[[2, 5, 3, 4], [10, 7, 8, 11], [14, 1, 0, 13], [6, 9, 15, 12]]
FINAL STATE:
[[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 0]]
HEURISTIC:  Manhattan_Distance
MOVES: 
R U L L D L D R U R R D L L U L U R U L D R D L D R R R 
PATH LENGTH: 29
NODES EXPANDED: 26014
EXECUTION TIME:  1 seconds
TRAVERSED STATES: 
2 5 3 4 

10 7 8 11 

14 1 0 13 

6 9 15 12 

  |    
  V    
2 5 3 4 

10 7 8 11 

14 1 13 0 

6 9 15 12 

  |    
  V    
2 5 3 4 

10 7 8 0 

14 1 13 11 

6 9 15 12 

  |    
  V    
2 5 3 4 

10 7 0 8 

14 1 13 11 

6 9 15 12 

  |    
  V    
2 5 3 4 

10 0 7 8 

14 1 13 11 

6 9 15 12 

  |    
  V    
2 5 3 4 

10 1 7 8 

14 0 13 11 

6 9 15 12 

  |    
  V    
2 5 3 4 

10 1 7 8 

0 14 13 11 

6 9 15 12 

  |    
  V    
2 5 3 4 

10 1 7 8 

6 14 13 11 

0 9 15 12 

  |    
  V    
2 5 3 4 

10 1 7 8 

6 14 13 11 

9 0 15 12 

  |    
  V    
2 5 3 4 

10 1 7 8 

6 0 13 11 

9 14 15 12 

  |    
  V    
2 5 3 4 

10 1 7 8 

6 13 0 11 

9 14 15 12 

  |    
  V    
2 5 3 4 

10 1 7 8 

6 13 11 0 

9 14 15 12 

  |    
  V    
2 5 3 4 

10 1 7 8 

6 13 11 12 

9 14 15 0 

  |    
  V    
2 5 3 4 

10 1 7 8 

6 13 11 12 

9 14 0 15 

  |    
  V    
2 5 3 4 

10 1 7 8 

6 13 11 12 

9 0 14 15 

  |    
  V    
2 5 3 4 

10 1 7 8 

6 0 11 12 

9 13 14 15 

  |    
  V    
2 5 3 4 

10 1 7 8 

0 6 11 12 

9 13 14 15 

  |    
  V    
2 5 3 4 

0 1 7 8 

10 6 11 12 

9 13 14 15 

  |    
  V    
2 5 3 4 

1 0 7 8 

10 6 11 12 

9 13 14 15 

  |    
  V    
2 0 3 4 

1 5 7 8 

10 6 11 12 

9 13 14 15 

  |    
  V    
0 2 3 4 

1 5 7 8 

10 6 11 12 

9 13 14 15 

  |    
  V    
1 2 3 4 

0 5 7 8 

10 6 11 12 

9 13 14 15 

  |    
  V    
1 2 3 4 

5 0 7 8 

10 6 11 12 

9 13 14 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

10 0 11 12 

9 13 14 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

0 10 11 12 

9 13 14 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 12 

0 13 14 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 12 

13 0 14 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 12 

13 14 0 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 12 

13 14 15 0 

[END Solution]

