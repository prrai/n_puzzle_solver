[Begin Solution]

[Program inputs]
ALGORITHM => IDA*
GAME => 15 puzzle
INPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/randomness_10/in_3.txt
OUTPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/randomness_10/out_3.txt
INITIAL STATE: 
[[1, 2, 3, 4], [5, 0, 6, 8], [9, 10, 7, 12], [13, 14, 11, 15]]
FINAL STATE:
[[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 0]]
HEURISTIC:  Manhattan_Distance
MOVES: 
R D D R 
PATH LENGTH: 5
NODES EXPANDED: 4
EXECUTION TIME:  0 seconds
TRAVERSED STATES: 
1 2 3 4 

5 0 6 8 

9 10 7 12 

13 14 11 15 

  |    
  V    
1 2 3 4 

5 6 0 8 

9 10 7 12 

13 14 11 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 0 12 

13 14 11 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 12 

13 14 0 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 12 

13 14 15 0 

[END Solution]

