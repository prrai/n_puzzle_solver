[Begin Solution]

[Program inputs]
ALGORITHM => IDA*
GAME => 15 puzzle
INPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/randomness_10/in_2.txt
OUTPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/randomness_10/out_2.txt
INITIAL STATE: 
[[0, 1, 3, 4], [6, 2, 7, 8], [5, 9, 10, 11], [13, 14, 15, 12]]
FINAL STATE:
[[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 0]]
HEURISTIC:  Manhattan_Distance
MOVES: 
R D L D R R R D 
PATH LENGTH: 9
NODES EXPANDED: 8
EXECUTION TIME:  0 seconds
TRAVERSED STATES: 
0 1 3 4 

6 2 7 8 

5 9 10 11 

13 14 15 12 

  |    
  V    
1 0 3 4 

6 2 7 8 

5 9 10 11 

13 14 15 12 

  |    
  V    
1 2 3 4 

6 0 7 8 

5 9 10 11 

13 14 15 12 

  |    
  V    
1 2 3 4 

0 6 7 8 

5 9 10 11 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 6 7 8 

0 9 10 11 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 0 10 11 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 0 11 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 0 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 12 

13 14 15 0 

[END Solution]

