[Begin Solution]

[Program inputs]
ALGORITHM => A*
GAME => 8 puzzle
INPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/test/in_7.txt
OUTPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/test/out_7.txt
INITIAL STATE: 
[[1, 5, 6], [7, 8, 4], [0, 2, 3]]
FINAL STATE:
[[1, 2, 3], [4, 5, 6], [7, 8, 0]]
HEURISTIC:  Manhattan_Distance
MOVES: 
R U R D L L U R R U L D D R U U L D R D 
PATH LENGTH: 21
NODES EXPANDED: 527
EXECUTION TIME:  0 seconds
TRAVERSED STATES: 
1 5 6 

7 8 4 

0 2 3 

  |    
  V    
1 5 6 

7 8 4 

2 0 3 

  |    
  V    
1 5 6 

7 0 4 

2 8 3 

  |    
  V    
1 5 6 

7 4 0 

2 8 3 

  |    
  V    
1 5 6 

7 4 3 

2 8 0 

  |    
  V    
1 5 6 

7 4 3 

2 0 8 

  |    
  V    
1 5 6 

7 4 3 

0 2 8 

  |    
  V    
1 5 6 

0 4 3 

7 2 8 

  |    
  V    
1 5 6 

4 0 3 

7 2 8 

  |    
  V    
1 5 6 

4 3 0 

7 2 8 

  |    
  V    
1 5 0 

4 3 6 

7 2 8 

  |    
  V    
1 0 5 

4 3 6 

7 2 8 

  |    
  V    
1 3 5 

4 0 6 

7 2 8 

  |    
  V    
1 3 5 

4 2 6 

7 0 8 

  |    
  V    
1 3 5 

4 2 6 

7 8 0 

  |    
  V    
1 3 5 

4 2 0 

7 8 6 

  |    
  V    
1 3 0 

4 2 5 

7 8 6 

  |    
  V    
1 0 3 

4 2 5 

7 8 6 

  |    
  V    
1 2 3 

4 0 5 

7 8 6 

  |    
  V    
1 2 3 

4 5 0 

7 8 6 

  |    
  V    
1 2 3 

4 5 6 

7 8 0 

HEURISTIC:  Misplaced_Tiles
MOVES: 
R U R D L L U R R U L D D R U U L D R D 
PATH LENGTH: 21
NODES EXPANDED: 3526
EXECUTION TIME:  0 seconds
TRAVERSED STATES: 
1 5 6 

7 8 4 

0 2 3 

  |    
  V    
1 5 6 

7 8 4 

2 0 3 

  |    
  V    
1 5 6 

7 0 4 

2 8 3 

  |    
  V    
1 5 6 

7 4 0 

2 8 3 

  |    
  V    
1 5 6 

7 4 3 

2 8 0 

  |    
  V    
1 5 6 

7 4 3 

2 0 8 

  |    
  V    
1 5 6 

7 4 3 

0 2 8 

  |    
  V    
1 5 6 

0 4 3 

7 2 8 

  |    
  V    
1 5 6 

4 0 3 

7 2 8 

  |    
  V    
1 5 6 

4 3 0 

7 2 8 

  |    
  V    
1 5 0 

4 3 6 

7 2 8 

  |    
  V    
1 0 5 

4 3 6 

7 2 8 

  |    
  V    
1 3 5 

4 0 6 

7 2 8 

  |    
  V    
1 3 5 

4 2 6 

7 0 8 

  |    
  V    
1 3 5 

4 2 6 

7 8 0 

  |    
  V    
1 3 5 

4 2 0 

7 8 6 

  |    
  V    
1 3 0 

4 2 5 

7 8 6 

  |    
  V    
1 0 3 

4 2 5 

7 8 6 

  |    
  V    
1 2 3 

4 0 5 

7 8 6 

  |    
  V    
1 2 3 

4 5 0 

7 8 6 

  |    
  V    
1 2 3 

4 5 6 

7 8 0 

[END Solution]

