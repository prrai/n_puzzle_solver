[Begin Solution]

[Program inputs]
ALGORITHM => A*
GAME => 8 puzzle
INPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/test/in_10.txt
OUTPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/test/out_10.txt
INITIAL STATE: 
[[1, 2, 3], [7, 0, 8], [6, 4, 5]]
FINAL STATE:
[[1, 2, 3], [4, 5, 6], [7, 8, 0]]
HEURISTIC:  Manhattan_Distance
MOVES: 
D L U R D R U L D R 
PATH LENGTH: 11
NODES EXPANDED: 18
EXECUTION TIME:  0 seconds
TRAVERSED STATES: 
1 2 3 

7 0 8 

6 4 5 

  |    
  V    
1 2 3 

7 4 8 

6 0 5 

  |    
  V    
1 2 3 

7 4 8 

0 6 5 

  |    
  V    
1 2 3 

0 4 8 

7 6 5 

  |    
  V    
1 2 3 

4 0 8 

7 6 5 

  |    
  V    
1 2 3 

4 6 8 

7 0 5 

  |    
  V    
1 2 3 

4 6 8 

7 5 0 

  |    
  V    
1 2 3 

4 6 0 

7 5 8 

  |    
  V    
1 2 3 

4 0 6 

7 5 8 

  |    
  V    
1 2 3 

4 5 6 

7 0 8 

  |    
  V    
1 2 3 

4 5 6 

7 8 0 

HEURISTIC:  Misplaced_Tiles
MOVES: 
D L U R R D L U R D 
PATH LENGTH: 11
NODES EXPANDED: 56
EXECUTION TIME:  0 seconds
TRAVERSED STATES: 
1 2 3 

7 0 8 

6 4 5 

  |    
  V    
1 2 3 

7 4 8 

6 0 5 

  |    
  V    
1 2 3 

7 4 8 

0 6 5 

  |    
  V    
1 2 3 

0 4 8 

7 6 5 

  |    
  V    
1 2 3 

4 0 8 

7 6 5 

  |    
  V    
1 2 3 

4 8 0 

7 6 5 

  |    
  V    
1 2 3 

4 8 5 

7 6 0 

  |    
  V    
1 2 3 

4 8 5 

7 0 6 

  |    
  V    
1 2 3 

4 0 5 

7 8 6 

  |    
  V    
1 2 3 

4 5 0 

7 8 6 

  |    
  V    
1 2 3 

4 5 6 

7 8 0 

[END Solution]

