[Begin Solution]

[Program inputs]
ALGORITHM => A*
GAME => 8 puzzle
INPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/test/in_3.txt
OUTPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/test/out_3.txt
INITIAL STATE: 
[[0, 2, 3], [1, 5, 8], [4, 6, 7]]
FINAL STATE:
[[1, 2, 3], [4, 5, 6], [7, 8, 0]]
HEURISTIC:  Manhattan_Distance
MOVES: 
D R D R U L L D R R 
PATH LENGTH: 11
NODES EXPANDED: 16
EXECUTION TIME:  0 seconds
TRAVERSED STATES: 
0 2 3 

1 5 8 

4 6 7 

  |    
  V    
1 2 3 

0 5 8 

4 6 7 

  |    
  V    
1 2 3 

5 0 8 

4 6 7 

  |    
  V    
1 2 3 

5 6 8 

4 0 7 

  |    
  V    
1 2 3 

5 6 8 

4 7 0 

  |    
  V    
1 2 3 

5 6 0 

4 7 8 

  |    
  V    
1 2 3 

5 0 6 

4 7 8 

  |    
  V    
1 2 3 

0 5 6 

4 7 8 

  |    
  V    
1 2 3 

4 5 6 

0 7 8 

  |    
  V    
1 2 3 

4 5 6 

7 0 8 

  |    
  V    
1 2 3 

4 5 6 

7 8 0 

HEURISTIC:  Misplaced_Tiles
MOVES: 
D R D R U L L D R R 
PATH LENGTH: 11
NODES EXPANDED: 42
EXECUTION TIME:  0 seconds
TRAVERSED STATES: 
0 2 3 

1 5 8 

4 6 7 

  |    
  V    
1 2 3 

0 5 8 

4 6 7 

  |    
  V    
1 2 3 

5 0 8 

4 6 7 

  |    
  V    
1 2 3 

5 6 8 

4 0 7 

  |    
  V    
1 2 3 

5 6 8 

4 7 0 

  |    
  V    
1 2 3 

5 6 0 

4 7 8 

  |    
  V    
1 2 3 

5 0 6 

4 7 8 

  |    
  V    
1 2 3 

0 5 6 

4 7 8 

  |    
  V    
1 2 3 

4 5 6 

0 7 8 

  |    
  V    
1 2 3 

4 5 6 

7 0 8 

  |    
  V    
1 2 3 

4 5 6 

7 8 0 

[END Solution]

