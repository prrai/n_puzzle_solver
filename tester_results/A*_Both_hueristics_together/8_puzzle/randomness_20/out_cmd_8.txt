[Begin Solution]

[Program inputs]
ALGORITHM => A*
GAME => 8 puzzle
INPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/test/in_8.txt
OUTPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/test/out_8.txt
INITIAL STATE: 
[[7, 3, 0], [8, 1, 5], [4, 2, 6]]
FINAL STATE:
[[1, 2, 3], [4, 5, 6], [7, 8, 0]]
HEURISTIC:  Manhattan_Distance
MOVES: 
L D D L U U R D D L U R R D 
PATH LENGTH: 15
NODES EXPANDED: 37
EXECUTION TIME:  0 seconds
TRAVERSED STATES: 
7 3 0 

8 1 5 

4 2 6 

  |    
  V    
7 0 3 

8 1 5 

4 2 6 

  |    
  V    
7 1 3 

8 0 5 

4 2 6 

  |    
  V    
7 1 3 

8 2 5 

4 0 6 

  |    
  V    
7 1 3 

8 2 5 

0 4 6 

  |    
  V    
7 1 3 

0 2 5 

8 4 6 

  |    
  V    
0 1 3 

7 2 5 

8 4 6 

  |    
  V    
1 0 3 

7 2 5 

8 4 6 

  |    
  V    
1 2 3 

7 0 5 

8 4 6 

  |    
  V    
1 2 3 

7 4 5 

8 0 6 

  |    
  V    
1 2 3 

7 4 5 

0 8 6 

  |    
  V    
1 2 3 

0 4 5 

7 8 6 

  |    
  V    
1 2 3 

4 0 5 

7 8 6 

  |    
  V    
1 2 3 

4 5 0 

7 8 6 

  |    
  V    
1 2 3 

4 5 6 

7 8 0 

HEURISTIC:  Misplaced_Tiles
MOVES: 
L D D L U U R D D L U R R D 
PATH LENGTH: 15
NODES EXPANDED: 207
EXECUTION TIME:  0 seconds
TRAVERSED STATES: 
7 3 0 

8 1 5 

4 2 6 

  |    
  V    
7 0 3 

8 1 5 

4 2 6 

  |    
  V    
7 1 3 

8 0 5 

4 2 6 

  |    
  V    
7 1 3 

8 2 5 

4 0 6 

  |    
  V    
7 1 3 

8 2 5 

0 4 6 

  |    
  V    
7 1 3 

0 2 5 

8 4 6 

  |    
  V    
0 1 3 

7 2 5 

8 4 6 

  |    
  V    
1 0 3 

7 2 5 

8 4 6 

  |    
  V    
1 2 3 

7 0 5 

8 4 6 

  |    
  V    
1 2 3 

7 4 5 

8 0 6 

  |    
  V    
1 2 3 

7 4 5 

0 8 6 

  |    
  V    
1 2 3 

0 4 5 

7 8 6 

  |    
  V    
1 2 3 

4 0 5 

7 8 6 

  |    
  V    
1 2 3 

4 5 0 

7 8 6 

  |    
  V    
1 2 3 

4 5 6 

7 8 0 

[END Solution]

