[Begin Solution]

[Program inputs]
ALGORITHM => A*
GAME => 8 puzzle
INPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/test/in_6.txt
OUTPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/test/out_6.txt
INITIAL STATE: 
[[0, 1, 6], [4, 3, 2], [7, 5, 8]]
FINAL STATE:
[[1, 2, 3], [4, 5, 6], [7, 8, 0]]
HEURISTIC:  Manhattan_Distance
MOVES: 
R D R U L D D R 
PATH LENGTH: 9
NODES EXPANDED: 10
EXECUTION TIME:  0 seconds
TRAVERSED STATES: 
0 1 6 

4 3 2 

7 5 8 

  |    
  V    
1 0 6 

4 3 2 

7 5 8 

  |    
  V    
1 3 6 

4 0 2 

7 5 8 

  |    
  V    
1 3 6 

4 2 0 

7 5 8 

  |    
  V    
1 3 0 

4 2 6 

7 5 8 

  |    
  V    
1 0 3 

4 2 6 

7 5 8 

  |    
  V    
1 2 3 

4 0 6 

7 5 8 

  |    
  V    
1 2 3 

4 5 6 

7 0 8 

  |    
  V    
1 2 3 

4 5 6 

7 8 0 

HEURISTIC:  Misplaced_Tiles
MOVES: 
R D R U L D D R 
PATH LENGTH: 9
NODES EXPANDED: 18
EXECUTION TIME:  0 seconds
TRAVERSED STATES: 
0 1 6 

4 3 2 

7 5 8 

  |    
  V    
1 0 6 

4 3 2 

7 5 8 

  |    
  V    
1 3 6 

4 0 2 

7 5 8 

  |    
  V    
1 3 6 

4 2 0 

7 5 8 

  |    
  V    
1 3 0 

4 2 6 

7 5 8 

  |    
  V    
1 0 3 

4 2 6 

7 5 8 

  |    
  V    
1 2 3 

4 0 6 

7 5 8 

  |    
  V    
1 2 3 

4 5 6 

7 0 8 

  |    
  V    
1 2 3 

4 5 6 

7 8 0 

[END Solution]

