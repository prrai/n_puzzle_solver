[Begin Solution]

[Program inputs]
ALGORITHM => A*
GAME => 15 puzzle
INPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/test/in_6.txt
OUTPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/test/out_6.txt
INITIAL STATE: 
[[1, 2, 3, 4], [5, 6, 7, 0], [13, 10, 11, 8], [14, 9, 15, 12]]
FINAL STATE:
[[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 0]]
HEURISTIC:  Manhattan_Distance
MOVES: 
D L L D L U R R R D 
PATH LENGTH: 11
NODES EXPANDED: 24
EXECUTION TIME:  0 seconds
TRAVERSED STATES: 
1 2 3 4 

5 6 7 0 

13 10 11 8 

14 9 15 12 

  |    
  V    
1 2 3 4 

5 6 7 8 

13 10 11 0 

14 9 15 12 

  |    
  V    
1 2 3 4 

5 6 7 8 

13 10 0 11 

14 9 15 12 

  |    
  V    
1 2 3 4 

5 6 7 8 

13 0 10 11 

14 9 15 12 

  |    
  V    
1 2 3 4 

5 6 7 8 

13 9 10 11 

14 0 15 12 

  |    
  V    
1 2 3 4 

5 6 7 8 

13 9 10 11 

0 14 15 12 

  |    
  V    
1 2 3 4 

5 6 7 8 

0 9 10 11 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 0 10 11 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 0 11 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 0 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 12 

13 14 15 0 

HEURISTIC:  Misplaced_Tiles
MOVES: 
D L L D L U R R R D 
PATH LENGTH: 11
NODES EXPANDED: 34
EXECUTION TIME:  0 seconds
TRAVERSED STATES: 
1 2 3 4 

5 6 7 0 

13 10 11 8 

14 9 15 12 

  |    
  V    
1 2 3 4 

5 6 7 8 

13 10 11 0 

14 9 15 12 

  |    
  V    
1 2 3 4 

5 6 7 8 

13 10 0 11 

14 9 15 12 

  |    
  V    
1 2 3 4 

5 6 7 8 

13 0 10 11 

14 9 15 12 

  |    
  V    
1 2 3 4 

5 6 7 8 

13 9 10 11 

14 0 15 12 

  |    
  V    
1 2 3 4 

5 6 7 8 

13 9 10 11 

0 14 15 12 

  |    
  V    
1 2 3 4 

5 6 7 8 

0 9 10 11 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 0 10 11 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 0 11 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 0 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 12 

13 14 15 0 

[END Solution]

