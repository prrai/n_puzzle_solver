[Begin Solution]

[Program inputs]
ALGORITHM => A*
GAME => 15 puzzle
INPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/test/in_2.txt
OUTPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/test/out_2.txt
INITIAL STATE: 
[[1, 2, 4, 7], [5, 6, 11, 3], [13, 9, 12, 10], [14, 15, 8, 0]]
FINAL STATE:
[[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 0]]
HEURISTIC:  Manhattan_Distance
MOVES: 
L U R D L L L U R R U R U L D R D D 
PATH LENGTH: 19
NODES EXPANDED: 57
EXECUTION TIME:  0 seconds
TRAVERSED STATES: 
1 2 4 7 

5 6 11 3 

13 9 12 10 

14 15 8 0 

  |    
  V    
1 2 4 7 

5 6 11 3 

13 9 12 10 

14 15 0 8 

  |    
  V    
1 2 4 7 

5 6 11 3 

13 9 0 10 

14 15 12 8 

  |    
  V    
1 2 4 7 

5 6 11 3 

13 9 10 0 

14 15 12 8 

  |    
  V    
1 2 4 7 

5 6 11 3 

13 9 10 8 

14 15 12 0 

  |    
  V    
1 2 4 7 

5 6 11 3 

13 9 10 8 

14 15 0 12 

  |    
  V    
1 2 4 7 

5 6 11 3 

13 9 10 8 

14 0 15 12 

  |    
  V    
1 2 4 7 

5 6 11 3 

13 9 10 8 

0 14 15 12 

  |    
  V    
1 2 4 7 

5 6 11 3 

0 9 10 8 

13 14 15 12 

  |    
  V    
1 2 4 7 

5 6 11 3 

9 0 10 8 

13 14 15 12 

  |    
  V    
1 2 4 7 

5 6 11 3 

9 10 0 8 

13 14 15 12 

  |    
  V    
1 2 4 7 

5 6 0 3 

9 10 11 8 

13 14 15 12 

  |    
  V    
1 2 4 7 

5 6 3 0 

9 10 11 8 

13 14 15 12 

  |    
  V    
1 2 4 0 

5 6 3 7 

9 10 11 8 

13 14 15 12 

  |    
  V    
1 2 0 4 

5 6 3 7 

9 10 11 8 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 6 0 7 

9 10 11 8 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 6 7 0 

9 10 11 8 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 0 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 12 

13 14 15 0 

HEURISTIC:  Misplaced_Tiles
MOVES: 
L U R D L L L U R R U R U L D R D D 
PATH LENGTH: 19
NODES EXPANDED: 787
EXECUTION TIME:  0 seconds
TRAVERSED STATES: 
1 2 4 7 

5 6 11 3 

13 9 12 10 

14 15 8 0 

  |    
  V    
1 2 4 7 

5 6 11 3 

13 9 12 10 

14 15 0 8 

  |    
  V    
1 2 4 7 

5 6 11 3 

13 9 0 10 

14 15 12 8 

  |    
  V    
1 2 4 7 

5 6 11 3 

13 9 10 0 

14 15 12 8 

  |    
  V    
1 2 4 7 

5 6 11 3 

13 9 10 8 

14 15 12 0 

  |    
  V    
1 2 4 7 

5 6 11 3 

13 9 10 8 

14 15 0 12 

  |    
  V    
1 2 4 7 

5 6 11 3 

13 9 10 8 

14 0 15 12 

  |    
  V    
1 2 4 7 

5 6 11 3 

13 9 10 8 

0 14 15 12 

  |    
  V    
1 2 4 7 

5 6 11 3 

0 9 10 8 

13 14 15 12 

  |    
  V    
1 2 4 7 

5 6 11 3 

9 0 10 8 

13 14 15 12 

  |    
  V    
1 2 4 7 

5 6 11 3 

9 10 0 8 

13 14 15 12 

  |    
  V    
1 2 4 7 

5 6 0 3 

9 10 11 8 

13 14 15 12 

  |    
  V    
1 2 4 7 

5 6 3 0 

9 10 11 8 

13 14 15 12 

  |    
  V    
1 2 4 0 

5 6 3 7 

9 10 11 8 

13 14 15 12 

  |    
  V    
1 2 0 4 

5 6 3 7 

9 10 11 8 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 6 0 7 

9 10 11 8 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 6 7 0 

9 10 11 8 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 0 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 12 

13 14 15 0 

[END Solution]

