[Begin Solution]

[Program inputs]
ALGORITHM => A*
GAME => 15 puzzle
INPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/test/in_4.txt
OUTPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/test/out_4.txt
INITIAL STATE: 
[[1, 2, 3, 4], [5, 11, 15, 7], [10, 6, 14, 8], [9, 0, 13, 12]]
FINAL STATE:
[[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 0]]
HEURISTIC:  Manhattan_Distance
MOVES: 
R U U L D L D R R U U R D D 
PATH LENGTH: 15
NODES EXPANDED: 17
EXECUTION TIME:  0 seconds
TRAVERSED STATES: 
1 2 3 4 

5 11 15 7 

10 6 14 8 

9 0 13 12 

  |    
  V    
1 2 3 4 

5 11 15 7 

10 6 14 8 

9 13 0 12 

  |    
  V    
1 2 3 4 

5 11 15 7 

10 6 0 8 

9 13 14 12 

  |    
  V    
1 2 3 4 

5 11 0 7 

10 6 15 8 

9 13 14 12 

  |    
  V    
1 2 3 4 

5 0 11 7 

10 6 15 8 

9 13 14 12 

  |    
  V    
1 2 3 4 

5 6 11 7 

10 0 15 8 

9 13 14 12 

  |    
  V    
1 2 3 4 

5 6 11 7 

0 10 15 8 

9 13 14 12 

  |    
  V    
1 2 3 4 

5 6 11 7 

9 10 15 8 

0 13 14 12 

  |    
  V    
1 2 3 4 

5 6 11 7 

9 10 15 8 

13 0 14 12 

  |    
  V    
1 2 3 4 

5 6 11 7 

9 10 15 8 

13 14 0 12 

  |    
  V    
1 2 3 4 

5 6 11 7 

9 10 0 8 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 6 0 7 

9 10 11 8 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 6 7 0 

9 10 11 8 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 0 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 12 

13 14 15 0 

HEURISTIC:  Misplaced_Tiles
MOVES: 
R U U L D L D R R U U R D D 
PATH LENGTH: 15
NODES EXPANDED: 92
EXECUTION TIME:  0 seconds
TRAVERSED STATES: 
1 2 3 4 

5 11 15 7 

10 6 14 8 

9 0 13 12 

  |    
  V    
1 2 3 4 

5 11 15 7 

10 6 14 8 

9 13 0 12 

  |    
  V    
1 2 3 4 

5 11 15 7 

10 6 0 8 

9 13 14 12 

  |    
  V    
1 2 3 4 

5 11 0 7 

10 6 15 8 

9 13 14 12 

  |    
  V    
1 2 3 4 

5 0 11 7 

10 6 15 8 

9 13 14 12 

  |    
  V    
1 2 3 4 

5 6 11 7 

10 0 15 8 

9 13 14 12 

  |    
  V    
1 2 3 4 

5 6 11 7 

0 10 15 8 

9 13 14 12 

  |    
  V    
1 2 3 4 

5 6 11 7 

9 10 15 8 

0 13 14 12 

  |    
  V    
1 2 3 4 

5 6 11 7 

9 10 15 8 

13 0 14 12 

  |    
  V    
1 2 3 4 

5 6 11 7 

9 10 15 8 

13 14 0 12 

  |    
  V    
1 2 3 4 

5 6 11 7 

9 10 0 8 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 6 0 7 

9 10 11 8 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 6 7 0 

9 10 11 8 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 0 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 12 

13 14 15 0 

[END Solution]

