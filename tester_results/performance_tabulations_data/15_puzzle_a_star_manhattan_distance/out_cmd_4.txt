[Begin Solution]

[Program inputs]
ALGORITHM => A*
GAME => 15 puzzle
INPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/compare_2/in_4.txt
OUTPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/compare_2/out_4.txt
INITIAL STATE: 
[[1, 2, 8, 3], [5, 11, 6, 4], [0, 9, 7, 12], [13, 10, 14, 15]]
FINAL STATE:
[[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 0]]
HEURISTIC:  
Manhattan_Distance
MOVES: 
R U R U R D L D L D R R 
PATH LENGTH: 13
NODES EXPANDED: 20
EXECUTION TIME: 8.945942 milli-seconds
TRAVERSED STATES: 
1 2 8 3 

5 11 6 4 

0 9 7 12 

13 10 14 15 

  |    
  V    
1 2 8 3 

5 11 6 4 

9 0 7 12 

13 10 14 15 

  |    
  V    
1 2 8 3 

5 0 6 4 

9 11 7 12 

13 10 14 15 

  |    
  V    
1 2 8 3 

5 6 0 4 

9 11 7 12 

13 10 14 15 

  |    
  V    
1 2 0 3 

5 6 8 4 

9 11 7 12 

13 10 14 15 

  |    
  V    
1 2 3 0 

5 6 8 4 

9 11 7 12 

13 10 14 15 

  |    
  V    
1 2 3 4 

5 6 8 0 

9 11 7 12 

13 10 14 15 

  |    
  V    
1 2 3 4 

5 6 0 8 

9 11 7 12 

13 10 14 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 11 0 12 

13 10 14 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 0 11 12 

13 10 14 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 12 

13 0 14 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 12 

13 14 0 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 12 

13 14 15 0 

[END Solution]

