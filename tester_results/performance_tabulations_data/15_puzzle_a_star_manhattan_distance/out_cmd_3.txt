[Begin Solution]

[Program inputs]
ALGORITHM => A*
GAME => 15 puzzle
INPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/compare_2/in_3.txt
OUTPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/compare_2/out_3.txt
INITIAL STATE: 
[[2, 6, 0, 4], [1, 7, 3, 8], [5, 9, 15, 11], [13, 10, 14, 12]]
FINAL STATE:
[[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 0]]
HEURISTIC:  Manhattan_Distance
MOVES: 
D L U L D D R D R U R D 
PATH LENGTH: 13
NODES EXPANDED: 12
EXECUTION TIME: 2.455950 milli-seconds
TRAVERSED STATES: 
2 6 0 4 

1 7 3 8 

5 9 15 11 

13 10 14 12 

  |    
  V    
2 6 3 4 

1 7 0 8 

5 9 15 11 

13 10 14 12 

  |    
  V    
2 6 3 4 

1 0 7 8 

5 9 15 11 

13 10 14 12 

  |    
  V    
2 0 3 4 

1 6 7 8 

5 9 15 11 

13 10 14 12 

  |    
  V    
0 2 3 4 

1 6 7 8 

5 9 15 11 

13 10 14 12 

  |    
  V    
1 2 3 4 

0 6 7 8 

5 9 15 11 

13 10 14 12 

  |    
  V    
1 2 3 4 

5 6 7 8 

0 9 15 11 

13 10 14 12 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 0 15 11 

13 10 14 12 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 15 11 

13 0 14 12 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 15 11 

13 14 0 12 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 0 11 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 0 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 12 

13 14 15 0 

[END Solution]

