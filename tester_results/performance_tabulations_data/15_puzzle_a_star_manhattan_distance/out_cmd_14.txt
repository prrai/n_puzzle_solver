[Begin Solution]

[Program inputs]
ALGORITHM => A*
GAME => 15 puzzle
INPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/compare_2/in_14.txt
OUTPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/compare_2/out_14.txt
INITIAL STATE: 
[[1, 2, 3, 4], [5, 6, 7, 8], [13, 11, 14, 12], [10, 9, 15, 0]]
FINAL STATE:
[[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 0]]
HEURISTIC:  Manhattan_Distance
MOVES: 
L U L D L U R D R R 
PATH LENGTH: 11
NODES EXPANDED: 13
EXECUTION TIME: 7.371902 milli-seconds
TRAVERSED STATES: 
1 2 3 4 

5 6 7 8 

13 11 14 12 

10 9 15 0 

  |    
  V    
1 2 3 4 

5 6 7 8 

13 11 14 12 

10 9 0 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

13 11 0 12 

10 9 14 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

13 0 11 12 

10 9 14 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

13 9 11 12 

10 0 14 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

13 9 11 12 

0 10 14 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

0 9 11 12 

13 10 14 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 0 11 12 

13 10 14 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 12 

13 0 14 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 12 

13 14 0 15 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 12 

13 14 15 0 

[END Solution]

