[Begin Solution]

[Program inputs]
ALGORITHM => A*
GAME => 15 puzzle
INPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/compare_2/in_1.txt
OUTPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/compare_2/out_1.txt
INITIAL STATE: 
[[1, 3, 8, 4], [5, 0, 2, 11], [9, 6, 15, 7], [13, 10, 14, 12]]
FINAL STATE:
[[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 0]]
HEURISTIC:  Manhattan_Distance
MOVES: 
R U L D D D R U R U L D R D 
PATH LENGTH: 15
NODES EXPANDED: 25
EXECUTION TIME: 5.206108 milli-seconds
TRAVERSED STATES: 
1 3 8 4 

5 0 2 11 

9 6 15 7 

13 10 14 12 

  |    
  V    
1 3 8 4 

5 2 0 11 

9 6 15 7 

13 10 14 12 

  |    
  V    
1 3 0 4 

5 2 8 11 

9 6 15 7 

13 10 14 12 

  |    
  V    
1 0 3 4 

5 2 8 11 

9 6 15 7 

13 10 14 12 

  |    
  V    
1 2 3 4 

5 0 8 11 

9 6 15 7 

13 10 14 12 

  |    
  V    
1 2 3 4 

5 6 8 11 

9 0 15 7 

13 10 14 12 

  |    
  V    
1 2 3 4 

5 6 8 11 

9 10 15 7 

13 0 14 12 

  |    
  V    
1 2 3 4 

5 6 8 11 

9 10 15 7 

13 14 0 12 

  |    
  V    
1 2 3 4 

5 6 8 11 

9 10 0 7 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 6 8 11 

9 10 7 0 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 6 8 0 

9 10 7 11 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 6 0 8 

9 10 7 11 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 0 11 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 0 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 12 

13 14 15 0 

[END Solution]

