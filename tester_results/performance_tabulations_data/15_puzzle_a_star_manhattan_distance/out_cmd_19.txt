[Begin Solution]

[Program inputs]
ALGORITHM => A*
GAME => 15 puzzle
INPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/compare_2/in_19.txt
OUTPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/compare_2/out_19.txt
INITIAL STATE: 
[[1, 3, 0, 4], [5, 2, 6, 8], [9, 10, 7, 11], [13, 14, 15, 12]]
FINAL STATE:
[[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 0]]
HEURISTIC:  Manhattan_Distance
MOVES: 
L D R D R D 
PATH LENGTH: 7
NODES EXPANDED: 6
EXECUTION TIME: 1.383066 milli-seconds
TRAVERSED STATES: 
1 3 0 4 

5 2 6 8 

9 10 7 11 

13 14 15 12 

  |    
  V    
1 0 3 4 

5 2 6 8 

9 10 7 11 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 0 6 8 

9 10 7 11 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 6 0 8 

9 10 7 11 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 0 11 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 0 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 12 

13 14 15 0 

[END Solution]

