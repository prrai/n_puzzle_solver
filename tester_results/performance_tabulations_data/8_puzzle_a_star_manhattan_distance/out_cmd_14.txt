[Begin Solution]

[Program inputs]
ALGORITHM => A*
GAME => 8 puzzle
INPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/compare/in_14.txt
OUTPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/compare/out_14.txt
INITIAL STATE: 
[[0, 1, 3], [4, 2, 5], [7, 8, 6]]
FINAL STATE:
[[1, 2, 3], [4, 5, 6], [7, 8, 0]]
HEURISTIC:  Manhattan_Distance
MOVES: 
R D R D 
PATH LENGTH: 5
NODES EXPANDED: 4
EXECUTION TIME: 3.818035 milli-seconds
TRAVERSED STATES: 
0 1 3 

4 2 5 

7 8 6 

  |    
  V    
1 0 3 

4 2 5 

7 8 6 

  |    
  V    
1 2 3 

4 0 5 

7 8 6 

  |    
  V    
1 2 3 

4 5 0 

7 8 6 

  |    
  V    
1 2 3 

4 5 6 

7 8 0 

[END Solution]

