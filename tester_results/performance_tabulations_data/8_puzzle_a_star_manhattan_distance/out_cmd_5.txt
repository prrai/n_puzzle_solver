[Begin Solution]

[Program inputs]
ALGORITHM => A*
GAME => 8 puzzle
INPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/compare/in_5.txt
OUTPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/compare/out_5.txt
INITIAL STATE: 
[[0, 3, 5], [1, 2, 6], [4, 7, 8]]
FINAL STATE:
[[1, 2, 3], [4, 5, 6], [7, 8, 0]]
HEURISTIC:  Manhattan_Distance
MOVES: 
D D R R U U L D R D 
PATH LENGTH: 11
NODES EXPANDED: 15
EXECUTION TIME: 1.976967 milli-seconds
TRAVERSED STATES: 
0 3 5 

1 2 6 

4 7 8 

  |    
  V    
1 3 5 

0 2 6 

4 7 8 

  |    
  V    
1 3 5 

4 2 6 

0 7 8 

  |    
  V    
1 3 5 

4 2 6 

7 0 8 

  |    
  V    
1 3 5 

4 2 6 

7 8 0 

  |    
  V    
1 3 5 

4 2 0 

7 8 6 

  |    
  V    
1 3 0 

4 2 5 

7 8 6 

  |    
  V    
1 0 3 

4 2 5 

7 8 6 

  |    
  V    
1 2 3 

4 0 5 

7 8 6 

  |    
  V    
1 2 3 

4 5 0 

7 8 6 

  |    
  V    
1 2 3 

4 5 6 

7 8 0 

[END Solution]

