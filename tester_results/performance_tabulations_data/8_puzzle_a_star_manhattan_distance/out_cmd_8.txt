[Begin Solution]

[Program inputs]
ALGORITHM => A*
GAME => 8 puzzle
INPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/compare/in_8.txt
OUTPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/compare/out_8.txt
INITIAL STATE: 
[[2, 5, 3], [1, 0, 6], [4, 7, 8]]
FINAL STATE:
[[1, 2, 3], [4, 5, 6], [7, 8, 0]]
HEURISTIC:  Manhattan_Distance
MOVES: 
U L D D R R 
PATH LENGTH: 7
NODES EXPANDED: 6
EXECUTION TIME: 0.913143 milli-seconds
TRAVERSED STATES: 
2 5 3 

1 0 6 

4 7 8 

  |    
  V    
2 0 3 

1 5 6 

4 7 8 

  |    
  V    
0 2 3 

1 5 6 

4 7 8 

  |    
  V    
1 2 3 

0 5 6 

4 7 8 

  |    
  V    
1 2 3 

4 5 6 

0 7 8 

  |    
  V    
1 2 3 

4 5 6 

7 0 8 

  |    
  V    
1 2 3 

4 5 6 

7 8 0 

[END Solution]

