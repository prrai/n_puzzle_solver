[Begin Solution]

[Program inputs]
ALGORITHM => A*
GAME => 8 puzzle
INPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/compare/in_5.txt
OUTPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/compare/out_5.txt
INITIAL STATE: 
[[5, 2, 3], [1, 0, 4], [7, 8, 6]]
FINAL STATE:
[[1, 2, 3], [4, 5, 6], [7, 8, 0]]
HEURISTIC:  Misplaced_Tiles
MOVES: 
R U L L D R U R D D 
PATH LENGTH: 11
NODES EXPANDED: 61
EXECUTION TIME: 19.859076 milli-seconds
TRAVERSED STATES: 
5 2 3 

1 0 4 

7 8 6 

  |    
  V    
5 2 3 

1 4 0 

7 8 6 

  |    
  V    
5 2 0 

1 4 3 

7 8 6 

  |    
  V    
5 0 2 

1 4 3 

7 8 6 

  |    
  V    
0 5 2 

1 4 3 

7 8 6 

  |    
  V    
1 5 2 

0 4 3 

7 8 6 

  |    
  V    
1 5 2 

4 0 3 

7 8 6 

  |    
  V    
1 0 2 

4 5 3 

7 8 6 

  |    
  V    
1 2 0 

4 5 3 

7 8 6 

  |    
  V    
1 2 3 

4 5 0 

7 8 6 

  |    
  V    
1 2 3 

4 5 6 

7 8 0 

[END Solution]

