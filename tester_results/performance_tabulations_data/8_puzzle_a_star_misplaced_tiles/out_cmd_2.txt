[Begin Solution]

[Program inputs]
ALGORITHM => A*
GAME => 8 puzzle
INPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/compare/in_2.txt
OUTPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/compare/out_2.txt
INITIAL STATE: 
[[4, 1, 0], [5, 3, 2], [7, 8, 6]]
FINAL STATE:
[[1, 2, 3], [4, 5, 6], [7, 8, 0]]
HEURISTIC:  Misplaced_Tiles
MOVES: 
D L L U R R D D 
PATH LENGTH: 9
NODES EXPANDED: 14
EXECUTION TIME: 1.543045 milli-seconds
TRAVERSED STATES: 
4 1 0 

5 3 2 

7 8 6 

  |    
  V    
4 1 2 

5 3 0 

7 8 6 

  |    
  V    
4 1 2 

5 0 3 

7 8 6 

  |    
  V    
4 1 2 

0 5 3 

7 8 6 

  |    
  V    
0 1 2 

4 5 3 

7 8 6 

  |    
  V    
1 0 2 

4 5 3 

7 8 6 

  |    
  V    
1 2 0 

4 5 3 

7 8 6 

  |    
  V    
1 2 3 

4 5 0 

7 8 6 

  |    
  V    
1 2 3 

4 5 6 

7 8 0 

[END Solution]

