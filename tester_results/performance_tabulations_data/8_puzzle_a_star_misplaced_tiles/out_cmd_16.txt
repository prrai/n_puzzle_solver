[Begin Solution]

[Program inputs]
ALGORITHM => A*
GAME => 8 puzzle
INPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/compare/in_16.txt
OUTPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/compare/out_16.txt
INITIAL STATE: 
[[1, 3, 0], [5, 2, 6], [4, 7, 8]]
FINAL STATE:
[[1, 2, 3], [4, 5, 6], [7, 8, 0]]
HEURISTIC:  Misplaced_Tiles
MOVES: 
L D L D R R 
PATH LENGTH: 7
NODES EXPANDED: 6
EXECUTION TIME: 6.297827 milli-seconds
TRAVERSED STATES: 
1 3 0 

5 2 6 

4 7 8 

  |    
  V    
1 0 3 

5 2 6 

4 7 8 

  |    
  V    
1 2 3 

5 0 6 

4 7 8 

  |    
  V    
1 2 3 

0 5 6 

4 7 8 

  |    
  V    
1 2 3 

4 5 6 

0 7 8 

  |    
  V    
1 2 3 

4 5 6 

7 0 8 

  |    
  V    
1 2 3 

4 5 6 

7 8 0 

[END Solution]

