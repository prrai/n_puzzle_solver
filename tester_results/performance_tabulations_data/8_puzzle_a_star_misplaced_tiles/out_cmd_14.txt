[Begin Solution]

[Program inputs]
ALGORITHM => A*
GAME => 8 puzzle
INPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/compare/in_14.txt
OUTPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/compare/out_14.txt
INITIAL STATE: 
[[1, 2, 3], [7, 5, 6], [0, 8, 4]]
FINAL STATE:
[[1, 2, 3], [4, 5, 6], [7, 8, 0]]
HEURISTIC:  Misplaced_Tiles
MOVES: 
R R U L D L U R R D 
PATH LENGTH: 11
NODES EXPANDED: 64
EXECUTION TIME: 9.697914 milli-seconds
TRAVERSED STATES: 
1 2 3 

7 5 6 

0 8 4 

  |    
  V    
1 2 3 

7 5 6 

8 0 4 

  |    
  V    
1 2 3 

7 5 6 

8 4 0 

  |    
  V    
1 2 3 

7 5 0 

8 4 6 

  |    
  V    
1 2 3 

7 0 5 

8 4 6 

  |    
  V    
1 2 3 

7 4 5 

8 0 6 

  |    
  V    
1 2 3 

7 4 5 

0 8 6 

  |    
  V    
1 2 3 

0 4 5 

7 8 6 

  |    
  V    
1 2 3 

4 0 5 

7 8 6 

  |    
  V    
1 2 3 

4 5 0 

7 8 6 

  |    
  V    
1 2 3 

4 5 6 

7 8 0 

[END Solution]

