[Begin Solution]

[Program inputs]
ALGORITHM => A*
GAME => 15 puzzle
INPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/compare_2/in_12.txt
OUTPUT FILE => /Users/prasoon/Documents/books/AI/AI Assignment 1/compare_2/out_12.txt
INITIAL STATE: 
[[1, 2, 7, 3], [5, 10, 6, 4], [0, 9, 11, 8], [13, 14, 15, 12]]
FINAL STATE:
[[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 0]]
HEURISTIC:  
Misplaced_Tiles
MOVES: 
R U R U R D D D 
PATH LENGTH: 9
NODES EXPANDED: 8
EXECUTION TIME: 1.504898 milli-seconds
TRAVERSED STATES: 
1 2 7 3 

5 10 6 4 

0 9 11 8 

13 14 15 12 

  |    
  V    
1 2 7 3 

5 10 6 4 

9 0 11 8 

13 14 15 12 

  |    
  V    
1 2 7 3 

5 0 6 4 

9 10 11 8 

13 14 15 12 

  |    
  V    
1 2 7 3 

5 6 0 4 

9 10 11 8 

13 14 15 12 

  |    
  V    
1 2 0 3 

5 6 7 4 

9 10 11 8 

13 14 15 12 

  |    
  V    
1 2 3 0 

5 6 7 4 

9 10 11 8 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 6 7 0 

9 10 11 8 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 0 

13 14 15 12 

  |    
  V    
1 2 3 4 

5 6 7 8 

9 10 11 12 

13 14 15 0 

[END Solution]

