# CSE 537.01: Artificial Intelligence - Assignment 1
# PuzzleTester.py - Helper algorithm tester.
# Submitted By: Prasoon Rai
# SBU ID: 110921754
# This file was created for exhaustive testing of algorithms on various input combinations.

import sys
import os


def main():
    """
    Main() to generate solvable puzzles using puzzleGenerator.py and solve them using puzzleSolver.py
    The file takes puzzle type, randomization index, number of puzzles to solve, algorithm type and
    directory name to save results (gets created in current directory).
    :return: None
    """
    puzzle_type = sys.argv[1]
    randomization_moves = sys.argv[2]
    total_tests = int(sys.argv[3])
    algorithm_index = sys.argv[4]
    directory_path = sys.argv[5]
    if not os.path.exists(directory_path):
        os.makedirs(directory_path)
    test_path = os.path.join(os.getcwd(), directory_path)
    print(test_path)

    for i in range(0, total_tests):
        # Generate puzzle
        os.system('python -3 puzzleGenerator.py {0} {1} "{2}/in_{3}.txt"'.format(puzzle_type,
                                                                                 randomization_moves, test_path, i+1))
        # Solve puzzle
        os.system('python -3 puzzleSolver.py {0} {1} "{2}/in_{3}.txt" "{2}/out_{3}.txt" > "{2}/out_cmd_{3}.txt"'.
                  format(algorithm_index, puzzle_type, test_path, i+1))

if __name__ == '__main__':
    # Executes main()
    main()
