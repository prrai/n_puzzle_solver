# CSE 537.01: Artificial Intelligence - Assignment 1.
# puzzleSolverUtilities.py - Algorithm and puzzle utilities that solve the configuration and print results.
# Submitted By: Prasoon Rai.
# SBU ID: 110921754.


from __future__ import print_function
# Import heappush and heappop modules for priority queue implementation.
from heapq import heappush, heappop
# Import time module to estimate algorithm run time.
import time
# Import copy module to utilize deepcopy functionality.
import copy


# Global variables fore recursive IDA* algorithm: result path, state counter and timer.
ida_star_result = ''
ida_state_count = 0
ida_start_time = time.time()


class SearchAlgorithm:
    """
    Search Algorithm class:
    Contains methods that generate initial/goal boards and run search algorithms.
    """
    def __init__(self, data, output, puzzle_index, algorithm):
        """
        Initializes class variables, generates boards, runs the target algorithm and finally writes output to file.
        :param data: File contents of input puzzle.
        :param output: Output file object.
        :param puzzle_index: Puzzle type.
        :param algorithm: Algorithm to run.
        """
        self.puzzle_size = puzzle_index
        # Call initial board generator method to transform input data to a list of lists data structure (the board).
        self.initial_board = self.generate_initial_board(data)
        print("INITIAL STATE: ")
        print(self.initial_board)
        # Call goal board generator method to create a list of lists data structure (the board) based on puzzle size.
        self.goal_board = self.generate_goal_board()
        print("FINAL STATE:")
        print(self.goal_board)
        self.output_buffer = output
        if algorithm == "A*":
            # If A* is selected, run algorithm using the desired heuristic.
            path1 = self.run_a_star_algorithm("Manhattan_Distance")

            """
            1. Note: Since the assignment task aims at finally choosing one heuristic for run and output the result,
            the statement below has been purposefully commented out.
            2. Results for comparison of performance of two heuristics for same inputs
            are present at: /tester_results/A*_Both_hueristics_together
            3. Based on results, Manhattan_Distance was chosen as preferred heuristic, related data in report.
            """

            # path2 = self.run_a_star_algorithm("Misplaced_Tiles")
        else:
            path1 = self.run_memory_bound_iterative_deepening_a_star("Manhattan_Distance")

            """
            Note: Statement below purposefully commented out after determining Manhattan Distance as the
            chosen heuristics after comparative performance study of A*.
            """
            # path2 = self.run_memory_bound_iterative_deepening_a_star("Misplaced_Tiles")
        self.output_buffer.write(path1)
        # Close the buffer after writing result.
        self.output_buffer.close()

    def generate_initial_board(self, data):
        """
        Function to generate initial board.
        :param data: Puzzle contents from input file.
        :return: result as a list of lists data structure that represents initial board.
        """
        result = []
        data = data.replace('\r', '')
        data = data.split('\n')
        for item in data:
            temp_list = item.split(',')
            if len(temp_list) > 1:
                for token in range(0, len(temp_list)):
                    if temp_list[token] == '':
                        temp_list[token] = 0
                    else:
                        temp_list[token] = int(temp_list[token])
                result.append(temp_list)
        return result

    def generate_goal_board(self):
        """
        Function to generate goal board.
        Calculated based on puzzle size.
        :return: result as a list of lists data structure that represents goal board.
        """
        result = []
        ctr = 0
        for i in range(1, self.puzzle_size+1):
            temp_list = []
            for j in range(1, self.puzzle_size+1):
                ctr += 1
                if ctr == self.puzzle_size*self.puzzle_size:
                    temp_list.append(0)
                else:
                    temp_list.append(ctr)
            result.append(temp_list)
        return result

    def run_a_star_algorithm(self, heuristic):
        """
        Function to implement A* algorithm for the sought heuristic.
        Referred about A star algorithm functionality from class slides and wikipedia.
        [Source1: Class slides on Informed Search]
        [Source2: https://en.wikipedia.org/wiki/A*_search_algorithm]

        :param heuristic: Misplaced_tiles / Manhattan_Distance
        :return: The path traversed from initial to goal node. If no solution is found, empty string is returned.
        """
        print("HEURISTIC: ", heuristic)
        # Get the initial board.
        initial_state = Board(self.initial_board, self.goal_board, self.puzzle_size, heuristic)
        # Initialize a priority queue with f-cost as priority key
        open_list = PriorityQueue()
        # Maintain a dictionary of board's tile configuration (not the state object) to check if a configuration has
        # been seen earlier within the available state in frontier.
        open_dict = {}
        initial_state_tuple = tuple(tuple(iterate) for iterate in initial_state.board)
        open_dict[initial_state_tuple] = initial_state.f_n
        open_list.push_to_queue((initial_state.f_n, initial_state))
        # A set to capture the states already seen, so that we don't visit them again.
        explored_list = set()
        depth = 0
        # Initialize output string to empty and start the time.
        output_file_string = ''
        start_time = time.time()
        while open_list:
            # While we have elements in the frontier, we get the highest priority element (with lowest f-cost)
            current = open_list.pop_from_queue()[1]
            if current.board == current.goal_board:
                # If we have found the goal state, stop time counter, construct path, print all states to console.
                end_time = time.time()
                traversed_states, directions = current.construct_traversal_path()
                execution_time = float(end_time - start_time)
                print("MOVES: ")
                count = len(traversed_states) - 2
                for direction in reversed(directions):
                    print(direction, end=' ')
                    output_file_string += direction
                    if count:
                        count -= 1
                        output_file_string += ","
                # Print the data on path length, execution time and number of nodes expanded.
                print("\nPATH LENGTH: %d" % len(traversed_states))
                print("NODES EXPANDED: %d" % depth)
                print("EXECUTION TIME: %f milli-seconds" % (execution_time*1000))
                print("TRAVERSED STATES: ")
                count = len(traversed_states) - 1
                for state in reversed(traversed_states):
                    state.print_board()
                    if count:
                        count -= 1
                        print("  |    ")
                        print("  V    ")
                # Exit with success and return the output string to be written to file.
                return output_file_string
            depth += 1
            for state in current.actions(depth):
                # Evaluate each successor found by applying actions to be moved to open and closed data structures.
                state_board_tuple = tuple(tuple(iterate) for iterate in state.board)
                if state_board_tuple in open_dict or state_board_tuple in explored_list:
                    # If the current successor does not give the best path so far, ignore it.
                    if state.g_n <= current.g_n + 1:
                        continue
                state.g_n = current.g_n + 1
                state.f_n = state.g_n + state.h_n
                if state_board_tuple in explored_list:
                    # Remove from explored set
                    explored_list.remove(state_board_tuple)
                if state_board_tuple not in open_dict:
                    # Add the state to open list while board configuration tuple to open dictionary
                    open_dict[state_board_tuple] = state.f_n
                    open_list.push_to_queue((state.f_n, state))
                current_board_tuple = tuple(tuple(iterate) for iterate in current.board)
                # Add the current state to explored set.
                explored_list.add(current_board_tuple)
        else:
            # In case of failure, print to console.
            print('RESULT: Could not find a solution!')
        return ''

    def run_memory_bound_iterative_deepening_a_star(self, heuristic):
        """
        Function to implement memory bound iterative deepening A* algorithm.
        Referred about IDA* functionality from AIMA and pseudo code from wikipedia.
        [Source: https://en.wikipedia.org/wiki/Iterative_deepening_A*]

        :param heuristic: The heuristic to use for running IDA* : Misplaced_tiles / Manhattan_Distance
        :return: Path to traverse from initial to goal board, the same is written to output file.
        """
        print("HEURISTIC: ", heuristic)
        initial_state = Board(self.initial_board, self.goal_board, self.puzzle_size, heuristic)
        cut_off_heuristic = initial_state.h_n
        search_complete = False
        global ida_start_time
        ida_start_time = time.time()
        while search_complete is False:
            # Until we have found the goal node, we recursively explore the search tree upto a cut_off heuristic value
            # We retry by increasing the heuristic value in steps of 1.
            search_complete = self.recurse_memory_bound_iterative_deepening_a_star(initial_state, 0, cut_off_heuristic)
            if search_complete is False:
                cut_off_heuristic += 1
        return ida_star_result

    def recurse_memory_bound_iterative_deepening_a_star(self, state, g_n, cut_off_heuristic):
        """
        Recursive function called from driver function run_memory_bound_iterative_deepening_a_star()
        Explores the search Tree like A*, but with a cut-off bound on heuristic
        :param state: The current state to explore
        :param g_n: Path cost estimate
        :param cut_off_heuristic: cut-off heuristic value for the current recursion
        :return: True/False depending on successful goal finding
        """
        h_n = state.h_n
        output_file_string = ''
        global ida_state_count
        global ida_start_time
        if h_n == 0:
            # If h_n value is 0, it means we have found the goal, accumulate results and return with success
            # This portion of code is similar to A* implementation above.
            end_time = time.time()
            traversed_states, directions = state.construct_traversal_path()
            execution_time = float(end_time - ida_start_time)
            print("MOVES: ")
            count = len(traversed_states) - 2
            for direction in reversed(directions):
                print(direction, end=' ')
                output_file_string += direction
                if count:
                    count -= 1
                    output_file_string += ","
            print("\nPATH LENGTH: %d" % len(traversed_states))
            print("NODES EXPANDED: %d" % ida_state_count)
            print("EXECUTION TIME: %f milli-seconds" % (execution_time*1000))
            print("TRAVERSED STATES: ")
            count = len(traversed_states) - 1
            for state in reversed(traversed_states):
                state.print_board()
                if count:
                    count -= 1
                    print("  |    ")
                    print("  V    ")
            global ida_star_result
            ida_star_result = output_file_string
            return True
        f_n = g_n + h_n
        if f_n > cut_off_heuristic:
            return False
        """
        Note: The below priority queue is an optimization attempt to rank the successors on the basis of
        lower f_n values, instead of serially exploring all the successors. The space complexity does not increase
        since we are adding a queue of same size as the container of successors (Maximum = 4, total possible actions).

        With this change, the algorithm finds optimal paths after expanding much lesser nodes in several cases.
        Since we eventually explore all the successors, the algorithm is still complete.
        """
        sorted_neighbours = PriorityQueue()
        for neighbour in state.actions(state.g_n):
            sorted_neighbours.push_to_queue((neighbour.f_n, neighbour))
        while sorted_neighbours.length_of_queue() > 0:
            neighbour = sorted_neighbours.pop_from_queue()[1]
            ida_state_count += 1
            search_complete = self.recurse_memory_bound_iterative_deepening_a_star(neighbour, g_n + 1,
                                                                                   cut_off_heuristic)
            if search_complete is True:
                return True
        return False


class Board:
    """
    Board class:
    Contains methods to manage state and perform various state functions.
    """
    def __init__(self, initial_board, goal_board, puzzle_size, heuristic, depth=0, parent=None):
        """
        Initializes Board class variables and calculates cost estimates for each state.
        :param initial_board: Initial board configuration.
        :param goal_board: Goal board configuration.
        :param puzzle_size: Puzzle size of board.
        :param heuristic: Heuristic to apply: Manhattan_distance / Misplaced_Tiles.
        :param depth: Distance of current node from its parent.
        :param parent: Parent node for the current node.
        """
        self.board = initial_board
        self.goal_board = goal_board
        self.depth = depth
        self.parent = parent
        self.heuristic = heuristic
        self.puzzle_size = puzzle_size
        self.g_n = self.g_n()
        self.h_n = self.h_n()
        self.f_n = self.g_n + self.h_n

    def construct_traversal_path(self):
        """
        Function to trace the path from goal node to start node for the found solution.
        Starting with goal node, we find the direction of movement for successive parent nodes and append them in path.
        The calling function reverses the path and directions to arrive at source - destination directions and path.
        :return:
        """
        path = [self]
        state = self
        directions = list()
        while state.parent is not None:
            # Get the direction from state to its parent
            directions.append(self.get_direction_for_move(state, state.parent))
            state = state.parent
            path.append(state)
        return path, directions

    def get_direction_for_move(self, state_initial, state_final):
        """
        Function to get direction of movement from a one state to the other based on an action.
        :param state_initial: Initial board.
        :param state_final: Final board.
        :return: The direction to reach final board from initial board.
        """
        a, b = self.get_index_of_missing_tile(state_initial.board)
        p, q = self.get_index_of_missing_tile(state_final.board)
        # Based on the co-ordinates of missing tile, we determine the direction (move) that was chosen.
        if a == p and q - b == 1:
            return 'L'
        if b == q and a - p == 1:
            return 'D'
        if b == q and p - a == 1:
            return 'U'
        if a == p and b - q == 1:
            return 'R'

    def get_index_of_missing_tile(self, board):
        """
        Function to find the co-ordinates of missing tile.
        :param board: Board for which missing tile has to be reported.
        :return: Co-ordinates of the missing tile.
        """
        for i in range(0, self.puzzle_size):
            for j in range(0, self.puzzle_size):
                if board[i][j] == 0:
                    return i, j

    def actions(self, depth):
        """
        1. Function to apply the 4 possible actions {Right, Left, Down, Up} on the current state and produce
        the transformed state.
        2. The transformation is achieved by moving the blank tile in appropriate direction, i.e. swapping co-ordinates
        of blank tile with the tile in the target direction.
        3. Deepcopy has been used to avoid transformation to alter the parent board itself.
        :param depth: Depth of node.
        :return: A list of states which are the successors of current (the parent) node, formed by performing actions.
        """
        i, j = self.get_index_of_missing_tile(self.board)
        successors = []
        if i != 0:
            # If the blank tile can move up.
            transformed_board = copy.deepcopy(self.board)
            transformed_board[i][j], transformed_board[i-1][j] = transformed_board[i-1][j], transformed_board[i][j]
            temp_board = Board(transformed_board, self.goal_board, self.puzzle_size, self.heuristic, depth, self)
            successors.append(temp_board)
        if j != 0:
            # If the blank tile can move left.
            transformed_board = copy.deepcopy(self.board)
            transformed_board[i][j], transformed_board[i][j-1] = transformed_board[i][j-1], transformed_board[i][j]
            temp_board = Board(transformed_board, self.goal_board, self.puzzle_size, self.heuristic, depth, self)
            successors.append(temp_board)
        if i != self.puzzle_size - 1:
            # If the blank tile can move right.
            transformed_board = copy.deepcopy(self.board)
            transformed_board[i][j], transformed_board[i+1][j] = transformed_board[i+1][j], transformed_board[i][j]
            temp_board = Board(transformed_board, self.goal_board, self.puzzle_size, self.heuristic, depth, self)
            successors.append(temp_board)
        if j != self.puzzle_size - 1:
            # If the blank tile can move down.
            transformed_board = copy.deepcopy(self.board)
            transformed_board[i][j], transformed_board[i][j+1] = transformed_board[i][j+1], transformed_board[i][j]
            temp_board = Board(transformed_board, self.goal_board, self.puzzle_size, self.heuristic, depth, self)
            successors.append(temp_board)
        return successors

    def misplaced_tiles_heuristic(self):
        """
        Misplaced tile heuristic : If a tile is not at the same position as goal, add 1 to the sum, else add 0.
        [Source: https://heuristicswiki.wikispaces.com/Misplaced+Tiles]
        :return: Number of misplaced tiles for the current board as compared to goal board.
        """
        sum_misplaced = 0
        for i in range(0, self.puzzle_size):
            for j in range(0, self.puzzle_size):
                if self.board[i][j] != 0 and self.board[i][j] != self.goal_board[i][j]:
                    sum_misplaced += 1
        return sum_misplaced

    def manhattan_distance_heuristic(self):
        """
        Manhattan distance heuristic is the sum of manhattan distance of each element from goal.
        For a tile, it is the absolute distance between the co-ordinates in goal node vs current node.
        [Source: https://en.wikipedia.org/wiki/Taxicab_geometry#Measures_of_distances_in_chess]

        :return: Sum of Manhattan distance of all the elements from goal for the current board structure.
        """
        sum_manhattan = 0
        for i in range(0, self.puzzle_size):
            for j in range(0, self.puzzle_size):
                if self.board[i][j] != 0:
                    goal_x = int((self.board[i][j] - 1) / float(self.puzzle_size))
                    goal_y = (self.board[i][j] - 1) % self.puzzle_size
                    sum_manhattan += abs(i - goal_x) + abs(j - goal_y)
        return sum_manhattan

    def h_n(self):
        """
        Calculate heuristic value for each state based on chosen heuristic for algorithm.
        :return:
        """
        if self.heuristic == "Manhattan_Distance":
            return self.manhattan_distance_heuristic()
        if self.heuristic == "Misplaced_Tiles":
            return self.misplaced_tiles_heuristic()
        return 0

    def g_n(self):
        """
        The path cost estimate for each state.
        :return: Path cost.
        """
        return self.depth

    def print_board(self):
        """
        Print the board configuration to console.
        :return: None
        """
        for i in range(0, self.puzzle_size):
            for j in range(0, self.puzzle_size):
                print(self.board[i][j], end=' ')
            print('\n')


class PriorityQueue:
    """
    Implementation of priority queue using heap functions.
    The custom queue was implemented to have added functionality of getting the length of the queue.
    """

    def __init__(self):
        # Initialize queue list.
        self.priority_queue = list()

    def push_to_queue(self, board):
        # Push a board state to queue.
        heappush(self.priority_queue, board)

    def pop_from_queue(self):
        # Pop a board state from queue.
        front_board = heappop(self.priority_queue)
        return front_board

    def length_of_queue(self):
        # Get the number of board states currently enqueued.
        return len(self.priority_queue)
