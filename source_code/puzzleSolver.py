# CSE 537.01: Artificial Intelligence - Assignment 1
# PuzzleSolver.py - Solution File.
# Submitted By: Prasoon Rai
# SBU ID: 110921754


# Import sys library to utilize arguments parsing.
import sys
# Import SearchAlgorithm class to send parameters to solver algorithms.
from puzzleSolverUtilities import SearchAlgorithm


class PuzzleSolver:
    """
    Puzzle Solver class:
    Contains methods that handle command line validation, printing usage instructions and driving the algorithm.
    """
    def __init__(self):
        """
        Initialize variables and invoke validation of command line parameters.
        """
        self.algorithm = ''
        self.puzzle = ''
        self.input_file = ''
        self.output_file = ''
        self.input_data = ''
        self.validate_command_line_and_initialize_attributes()

    def validate_command_line_and_initialize_attributes(self):
        """
        Function to parse command line arguments and assign values to attributes.
        :return: None
        """
        print_usage_and_exit = False
        # Check if command line has 5 arguments, corresponds to executing algorithms. Exits for invalid choice.
        if len(sys.argv) == 5:
            # Choose algorithm based on command line input.
            self.algorithm = int(sys.argv[1])
            if self.algorithm == 1:
                self.algorithm = "A*"
            elif self.algorithm == 2:
                self.algorithm = "IDA*"
            else:
                print("\n[ERROR] Value for algorithm should be either 1 or 2. Aborting!\n")
                print_usage_and_exit = True

            self.puzzle = int(sys.argv[2])
            # Choose puzzle type based on command line input.
            if self.puzzle != 3 and self.puzzle != 4:
                # The algorithms are generalized in design to work for any nxn puzzle.
                # User gets warning for increasing values beyond 4, but program still tries to solve instead of exit.
                print("\n[WARNING] Value for puzzle should ideally be 3 or 4.\n")

            self.input_file = sys.argv[3]
            # Start an input file stream and read data, exits if fails.
            try:
                with open(self.input_file, mode='r') as data:
                    self.input_data = data.read()
            except EnvironmentError:
                print("\n[ERROR] Unable to read input file, or it does not exist. Aborting!\n")
                print_usage_and_exit = True

            # Start an output file stream for writing data, exits if fails. Stream is closed at the end of execution
            # by the target algorithm upon writing results.
            try:
                self.output_file = open(sys.argv[4], "w")
            except EnvironmentError:
                print("\n[ERROR] Unable to create output file at the provided location. Aborting!\n")
                print_usage_and_exit = True
        else:
            # Check if command line has 2 arguments, corresponds to executing help command. Prints detailed help.
            if len(sys.argv) == 2 and sys.argv[1] in {"--help", "--HELP", "--h", "--H", "-h", "-H", "-help", "-HELP"}:
                self.solver_help(True)
                sys.exit(0)
            # Improper command line arguments, exit and print usage part of the help.
            else:
                print("\n[ERROR] Incomplete arguments passed to the command line!\n")
                print_usage_and_exit = True
        # Choose to print usage or detailed help based on flag.
        if print_usage_and_exit:
            self.solver_help(False)
            sys.exit(2)

    def solver_help(self, help_flag=False):
        """
        Function to provide usage information to the command line.
        Can be accessed using --help / --help / -h / --h, like: puzzleSolver.py --help.
        Also gets called when user provides invalid input, as a usage helper.
        :return: None
        """
        if help_flag:
            # Prints detailed help if flag is set.
            print("\n*********************Puzzle Solver*********************")
            print("Program to solve 8 puzzle or 15 puzzle game using A* or\nmemory "
                  "bound Iterative Deepening A*(IDA*).\n")
            print("[Command line description]:")
            print("--help                   Displays this help menu")
            print("ALGORITHM_INDEX          1=> A* Algorithm")
            print("                         2=> Iterative Deepening A*(IDA*)")
            print("PUZZLE_TYPE              3=> 8-Puzzle game")
            print("                         4=> 15-Puzzle game")
            print("PATH_TO_INPUT_FILE       Full path to the text file containing solvable initial state of the puzzle")
            print("PATH_TO_OUTPUT_FILE      Full path to save the output solution file\n")

        # The usage part that gets executed whenever solver_help() is called.
        print("[Usage]:")
        print("1. Puzzle solver detailed help commandline: ")
        print("   [Example]: puzzleSolver.py --help\n")
        print("2. Puzzle solver main program executor: ")
        print("   [Syntax]: puzzleSolver.py <AlGORITHM_INDEX [1/2]> <PUZZLE_TYPE [3/4]> "
              "<PATH_TO_INPUT_FILE> <PATH_TO_OUTPUT_FILE>")
        print("   [Example]: puzzleSolver.py 1 3 input.txt output.txt\n")

    def run(self):
        """
        Core function that processes the input data and generates solution on the target algorithm.
        :return:
        """
        print("[Begin Solution]\n")
        print("[Program inputs]")
        print("ALGORITHM => {0}".format(self.algorithm))
        print("GAME => {0} puzzle".format(int(self.puzzle) * int(self.puzzle) - 1))
        print("INPUT FILE => {0}".format(self.input_file))
        print("OUTPUT FILE => {0}".format(sys.argv[4]))
        # Instantiate SearchAlgorithm class with input data, puzzle type, output file object and algorithm choice.
        SearchAlgorithm(self.input_data, self.output_file, self.puzzle, self.algorithm)
        print("[END Solution]\n")

if __name__ == '__main__':
    # Driver program to instantiate PuzzleSolver class.
    solver = PuzzleSolver()
    # Run the solver.
    solver.run()
